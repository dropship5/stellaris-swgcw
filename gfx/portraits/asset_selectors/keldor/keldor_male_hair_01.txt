# This is a template which multiple species can use. 
  
keldor_male_hair_01 = {
	default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"	
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/keldor/keldor_male_hair_01.dds"		
	}
}
  
keldor_male_hair_02 = {
	default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"	
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/keldor/keldor_male_hair_02.dds"		
	}
}
  
keldor_male_hair_03 = {
	default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"	
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/keldor/keldor_male_hair_03.dds"		
	}
}