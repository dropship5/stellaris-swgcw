# This is a template which multiple species can use. 

imperial_female_clothes_01 = {
	default = "gfx/models/portraits/imperial/gimperial_female_clothes_admiral_02.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/imperial/gimperial_female_clothes_admiral_02.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/imperial/gimperial_female_clothes_admiral_02.dds"
	}
	
	#pop scope
	pop = { #for a specific pop
		random = { 
			trigger = { always = yes }
			list = { 
				"gfx/models/portraits/imperial/gimperial_female_clothes_admiral_02.dds" 
				"gfx/models/portraits/imperial/gimperial_female_clothes_admiral_01.dds" 
				"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds"
				"gfx/models/portraits/imperial/gimperial_female_clothes_scientist_01.dds"
				"gfx/models/portraits/human/human_female_clothes_ruler.dds" 
				"gfx/models/portraits/human/human_female_clothes_ruler2.dds" 
				"gfx/models/portraits/human/human_female_clothes_governor.dds" 
			} 
		}
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		"gfx/models/portraits/imperial/gimperial_female_clothes_scientist_01.dds" = { leader_class = scientist }
		"gfx/models/portraits/imperial/gimperial_female_clothes_general_01.dds" = { leader_class = general }
		"gfx/models/portraits/imperial/gimperial_female_clothes_admiral_03.dds" = { leader_class = admiral }
		"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds" = { leader_class = governor }
	}

	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/imperial/gimperial_female_clothes_admiral_03.dds"

		"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds" = { owner = { has_generic_government = yes } }
		"gfx/models/portraits/imperial/gimperial_female_clothes_admiral_03.dds" = { owner = { has_militarist_government = yes } }
		"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds" = { owner = { has_spiritualist_government = yes } }
		"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds" = { owner = { has_pacifist_government = yes } }
		"gfx/models/portraits/imperial/gimperial_female_clothes_governor_01.dds" = { owner = { has_primitive_government = yes } }
		"gfx/models/portraits/imperial/gimperial_female_clothes_scientist_01.dds" = { owner = { has_materialist_government = yes } }
	}
}

imperial_female_hair_1 = { 
	default = "gfx/models/portraits/human/human_female_hair_black_style_01.dds"
	
	ruler = {
	default = "gfx/models/portraits/human/human_female_hair_black_style_01.dds"
		"gfx/models/portraits/human/human_female_hair_black_style_01.dds" = { owner = { has_militarist_government = yes } }
	}
}
