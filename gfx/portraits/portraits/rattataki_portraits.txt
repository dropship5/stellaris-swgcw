##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# rattataki
	rattataki_female_01 = { entity = "portrait_rattataki_female_01_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "no_texture" greeting_sound = "human_female_greetings_03"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_female_body_01.dds" }
	}
	rattataki_female_02 = { entity = "portrait_rattataki_female_02_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "no_texture" greeting_sound = "human_female_greetings_04"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_female_body_02.dds" }
	}
	rattataki_female_03 = { entity = "portrait_rattataki_female_03_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "no_texture" greeting_sound = "human_female_greetings_05"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_female_body_03.dds" }
	}
	rattataki_male_01 = { entity = "portrait_rattataki_male_01_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_03"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_male_body_01.dds" }
	}	
	rattataki_male_02 = { entity = "portrait_rattataki_male_02_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_04"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_male_body_02.dds" }
	}
	rattataki_male_03 = { entity = "portrait_rattataki_male_03_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_05"
		character_textures = { "gfx/models/portraits/rattataki/rattataki_male_body_03.dds" }
	}
}

portrait_groups = {
	rattataki = {
		default = rattataki_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					rattataki_male_01
					rattataki_male_02
					rattataki_male_03
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					rattataki_female_01
					rattataki_female_02
					rattataki_female_03
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					rattataki_female_01
					rattataki_female_02
					rattataki_female_03
					rattataki_male_01
					rattataki_male_02
					rattataki_male_03
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					rattataki_female_01
					rattataki_female_02
					rattataki_female_03
					rattataki_male_01
					rattataki_male_02
					rattataki_male_03
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					rattataki_female_01
					rattataki_female_02
					rattataki_female_03
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					rattataki_male_01
					rattataki_male_02
					rattataki_male_03
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					rattataki_female_01
					rattataki_female_02
					rattataki_female_03
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					rattataki_male_01
					rattataki_male_02
					rattataki_male_03
				}
			}
		}
	}
}