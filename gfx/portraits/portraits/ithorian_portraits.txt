
portraits = {
	ithorian = { entity = "portrait_reptilian_03_entity" clothes_selector = "no_texture" hair_selector = "no_texture" greeting_sound = "cute_reptilian_greetings"
		character_textures = {
			"gfx/models/portraits/ithorian/ithorian_male_body_01.dds"
			"gfx/models/portraits/ithorian/ithorian_male_body_02.dds"
			"gfx/models/portraits/ithorian/ithorian_male_body_03.dds"
			"gfx/models/portraits/ithorian/ithorian_male_body_04.dds"
		}
	}
}