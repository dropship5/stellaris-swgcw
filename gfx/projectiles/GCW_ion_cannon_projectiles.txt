#### Ion Cannons

projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "ion_cannon_m"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "ion_cannon_hit_entity"
	shield_hit_entity = "ion_cannon_hit_entity"
	muzzle_flash_entity = "ion_cannon_muzzle_entity"
	
	shield_impact = {
		size = small
		delay = 0.0
	}
	
	#ballistic specific
	entity = "ion_cannon_entity"
	speed = 120.0			#preferred speed of the projectile
	max_duration = 5.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}

projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "ion_cannon_h"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "ion_cannon_hit_entity"
	shield_hit_entity = "ion_cannon_hit_entity"
	muzzle_flash_entity = "ion_cannon_muzzle_entity"
	
	shield_impact = {
		size = small
		delay = 0.0
	}
	
	#ballistic specific
	entity = "heavy_ion_cannon_entity"
	speed = 120.0			#preferred speed of the projectile
	max_duration = 5.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}