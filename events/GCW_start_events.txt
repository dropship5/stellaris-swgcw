namespace = gcw_start

country_event = {
	id = gcw_start.1
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		set_global_flag = game_started 
		country_event = { id = gcw_start.2 days = 1 }
	}
}
country_event = {
	id = gcw_start.2
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		set_global_flag = game_started_day_1
	}
}

event = {
	id = gcw_start.3
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		every_ship = {
			limit = { is_ship_size = superweapon }
			destroy_fleet = fleet
		}
		random_country = { limit = { is_country_type = default } country_event = { id = gcw_start.1 days = 1 } } #set start flag
		every_country = { country_event = { id = gcw_start.25 days = 0 } } #set pops to homeworlds
		every_country = { limit = { is_country_type = default } country_event = { id = gcw_start.51 days = 0 } } #set starting military
		every_country = { limit = { is_country_type = default } every_owned_planet = { planet_event = { id = gcw_start.52 days = 0 } } } #set starting stations
		every_country = { limit = { is_country_type = default } country_event = { id = gcw_start.26 days = 0 } } #set all pops to core worlds allowed
		every_country = { limit = { is_country_type = default } country_event = { id = gcw_start.50 days = 0 } } #set citizenship rights
		set_global_flag = star_wars_galactic_civil_war
	}
}

event = {
	id = gcw_start.4
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		every_country = { country_event = { id = gcw_start.200 } }
	}
}

#set pops capital world
country_event = {
    id = gcw_start.25
	hide_window = yes
    is_triggered_only = yes
	trigger = {
		exists = capital_scope
	}
    immediate = { 		
		species = {
			set_species_homeworld = root.capital_scope
		}
    }
}

#set all pops to core worlds allowed
country_event = {
	id = gcw_start.26
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		set_policy = { policy = "core_worlds" option = "core_worlds_all" cooldown = no }
	}
}

#add mining/research stations to start systems
planet_event = {
	id = gcw_start.52
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		solar_system = {				
			every_system_planet = {
				limit = { 
					has_deposit_for = shipclass_mining_station 
					has_mining_station = no
				}
				create_mining_station = {
					owner = ROOT
				}
			}						
			every_system_planet = {
				limit = { 
					has_deposit_for = shipclass_research_station 
					has_research_station = no
				}
				create_research_station = {
					owner = ROOT
				}
			}
		}
	}
}

# starting military
country_event = {
	id = gcw_start.51
	hide_window = yes
    is_triggered_only = yes
	immediate = {
		create_fleet = {
			effect = {
				set_owner = prev
				
				while = {
					count = 2
					create_ship = {
						name = random
						random_existing_design = gcwcorvette
					}
					create_ship = {
						name = random
						random_existing_design = gcwcorvette
					}
					create_ship = {
						name = random
						random_existing_design = gcwcorvette
					}
					create_ship = {
						name = random
						random_existing_design = gcwcorvette
					}
					create_ship = {
						name = random
						random_existing_design = gcwfrigate
					}
					create_ship = {
						name = random
						random_existing_design = gcwfrigate
					}
					create_ship = {
						name = random
						random_existing_design = gcwfrigate
					}
					create_ship = {
						name = random
						random_existing_design = gcwcruiser
					}
					create_ship = {
						name = random
						random_existing_design = gcwcruiser
					}
					create_ship = {
						name = random
						random_existing_design = heavy_cruiser
					}
				}
				set_location = prev.capital_scope
			}
		}
	}
}

# set slavery/caste/etc code
country_event = {
    id = gcw_start.50
	hide_window = yes
    is_triggered_only = yes
    immediate = {
		every_country = {
			limit = { is_country_type = default }
			every_owned_pop_species = {
				set_living_standard = { country = root type = living_standard_normal cooldown = no }
				set_citizenship_type = { country = root type = citizenship_full cooldown = no }
			}
		}
	}
	after = {
		random_country = {
			limit = { has_country_flag = hutt_cartel }
			every_owned_pop_species = {
				limit = { NOT = { is_species = "Hutt" } }
				set_living_standard = {
					country = root
					type = living_standard_normal
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_caste_system
					cooldown = no
				}
			}
		}
		random_country = {
			limit = { has_country_flag = corporate_sector_authority }
			every_owned_pop_species = {
				limit = { NOT = { is_species = "Human" } }
				set_living_standard = {
					country = root
					type = living_standard_poor
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_caste_system
					cooldown = no
				}
			}
		}
		random_country = {
			limit = { has_country_flag = juvex_sector }
			every_owned_pop_species = {
				limit = { NOT = { is_species = "Human" } }
				set_living_standard = {
					country = root
					type = living_standard_poor
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_slavery
					cooldown = no
				}
			}
		}
		random_country = {
			limit = { has_country_flag = zygerrian_slave_guild }
			every_owned_pop_species = {
				limit = { NOT = { is_species = "Zygerrian" } }
				set_living_standard = {
					country = root
					type = living_standard_normal
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_caste_system
					cooldown = no
				}
			}
		}
		random_country = {
			limit = { has_country_flag = trandoshan_dominion }
			every_owned_pop_species = {
				limit = { NOT = { is_species = "Trandoshan" } }
				set_living_standard = {
					country = root
					type = living_standard_poor
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_slavery
					cooldown = no
				}
			}
		}
		random_country = {
			limit = { has_country_flag = galactic_empire }
			every_owned_pop_species = {
				limit = { 
					OR = { 
						is_species = "Twi'lek"
						is_species = "Mon Calamari" 
						is_species = "Wookie"
					} 
				}
				set_living_standard = {
					country = root
					type = living_standard_poor
					cooldown = no
				}
				set_citizenship_type = {
					country = root
					type = citizenship_slavery
					cooldown = no
				}
			}
		}
	}
}

#destroy all non shipyard planet spaceports
planet_event = {
    id = gcw_start.100
    hide_window = yes
	is_triggered_only = yes

    trigger = {
		has_spaceport = yes
		NOR = { 
			is_planet_class = pc_shipyard_hidden
			is_planet_class = pc_shipyard
		}
	}

    immediate = {
		destroy_fleet = spaceport
	}
}

# survey starting planets code
country_event = {
	id = gcw_start.200
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		every_owned_planet = {
			solar_system = {
				every_system_planet = {
					surveyed = {
						set_surveyed = yes
						surveyor = ROOT
					}
				}
			}
		}
	}
}