############################################
#
#	Angry Pirates 
#		by MrFreake
#
############################################

namespace = pirates

@initial_fleet = 50 # number of ships randomly divided amongst the four pirate fleets

#		KEY
#	Pirates getting stronger - 100
#	Pirate setup = 400
#	Pirate spawning = 500
#	Hostages - 600
#	Minerals - 700
#	Steal Research - 800
#	Steal ships - 900
#
#

event = {
	id = pirates.1
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {

	random_country = {
		limit = { 
			is_same_value = event_target:pirate_country 
				}
			random_owned_fleet = {
			
	
			random_list = {
			
				50 = { log = "pirates rolled a 0" }
				
				20 = { 	fleet_event = { id = pirates.700 } }#Pirates steal minerals
				
				#20 = { 	country_event = { id = pirates.3 } } #Pirates steal energy
				
				#20 = { country_event = { id = pirates.4 } } #Pirates steal research
				
				20 = { 	fleet_event = { id = pirates.601 } } #Pirates take hostages
				
				#10 = { 	country_event = { id = pirates.6 } } #Pirates grow and consolidate
		
					}
				}
			}
		}
	}
		
#####
#
#	So here's the plan dumdum. Pirates only get one type of ship (cruisers)
#	Every raid ships are spawned in four separate fleets across the galaxy. 
#	As the ships return from their raid they're destroyed, and the ships added back
# 	into the pool for the next spawn.
#	
#	#2 Rebelling planets can be taken over by pirates if pirates are strong enough
#
#
######

#ship_event = {				##### destroying pirate ships and adding to the variable
#		id = pirates.100
#		is_triggered_only = no
#		hide_window = yes
#		trigger = {
#		AND = {
#		#log = "pirates.100 trigger scope"
#		#print_scope_trigger = yes
#		is_ship_size = small_ship_swarm 
#		Root = { is_same_value = event_target:pirate_home } 
#		exists = orbit
#		Root.Owner = { is_same_value = event_target:pirate_country }
#		 } }
#		
#		immediate = {
#		log = "pre-variable scope effect"
#		print_scope_effect = yes
#		root = {
#			
#			log = "variable scope effect"	
##			print_scope_effect = yes
	#		change_variable = {
	#			which = pirate_fleet
	#			value = +1 }
	#		destroy_ship = this 
	#		
	#					}
	#				}
	#			}

country_event = {		# initial pirate base
		id = pirates.400
		is_triggered_only = yes
		hide_window = yes
		fire_only_once = yes
		
		immediate = {
		
		create_pirate_country = yes
		random_country = {
		limit = { is_pirate = yes }
		set_variable = {
				which = pirate_fleet
				value = @initial_fleet }
		random_system = {
		limit = { 
				AND = {
					NOT = { has_star_flag = hostile_system
                          has_star_flag = empire_cluster		}
				
					has_owner = no
				} }
		random_system_planet = { 	#set_timed_planet_flag = { flag = pirates_are_here days = 120 }
							set_planet_flag = pirates_were_here
							save_global_event_target_as = pirate_home
							log = "pirates.400 firing"
									
							}
							
						
						}
					 }
				}
			}
		

#planet_event = {
#	id = pirates.401
#	is_triggered_only = yes			#### I'm sorry, the princess is in another castle
#	hide_window = yes				#### Moving pirate base
#									
#	#trigger = {
#	#	has_planet_flag = pirates_were_here 
#	#	NOT = { has_planet_flag = pirates_are_here } 
#	#	any_fleet = { NOT = { has_fleet_flag = pirate_idle }
#	#	} }
#		
#		
#	immediate = {
#		this = {	log = "pirate flags scope effect"
#					print_scope_effect = yes
#					remove_planet_flag = pirates_were_here
#					set_planet_flag = pirate_base_relocated 
#					
#					}
#			random_system = {
#				limit = { 
#					AND = { 
#						has_owner = no	
#						NOT = { 	
#									has_star_flag = hostile_system
#									has_star_flag = empire_cluster		}
#								} 
#							}
#				set_star_flag = pirate_home
#
#				random_system_planet = { 
#				limit = { is_star = no }
#				set_timed_planet_flag = { flag = pirates_are_here days = 1800  }
#									set_planet_flag = pirates_were_here
#									
#									save_event_target_as = pirate_home	
#									log = "pirates.401 random_system_planet scope effect"
#									print_scope_effect = yes
#									
#							}	
#						}
#					
#				}
#			}
			
		

#country_event = {		#dumb spawning pirate ships (auto 100)
#		id = pirates.402
#		hide_window = yes
#		is_triggered_only = yes
#		
#		trigger = {	}
#		
#		immediate = {
#			random_country = {
#				limit = { is_same_value = event_target:pirate_country }
#					
#				random_planet = {
#					limit = { is_same_value = event_target:pirate_home }
#			
#					log = "pirates.402 step 3"
#					create_fleet = {
#						name = random
#						effect = {
#							set_owner = event_target:pirate_country
#							create_ship = {
#								name = random
#								design = "NAME_Swarm_Small"
#								graphical_culture = "swarm_01"
#									}
#							log = "create_fleet log"
#							assign_leader = random
#							set_fleet_flag = pirate_fleet
#							set_location = {
#								target = event_target:pirate_home
#								distance = 35
#								angle = random 
#											}
#							set_fleet_stance = aggressive
#							set_aggro_range = 500
#							set_aggro_range_measure_from = self
#				log = "create_fleet succeeded"
#							while = {
#								count = 100
#										
#												create_ship = {
#													name = random
#													design = "NAME_Swarm_Small"
#													graphical_culture = "swarm_01"
#
#													}
#
#										
#
#										}
#									}
#								}
#							}
#						}
#					}
#				}

country_event = {
		id = pirates.501			### pirates growing stronger
		title = GCW_pirates_event.501.name
		desc = GCW_pirates_event.501.desc
		picture = GFX_evt_pirate_base_stronger
		is_triggered_only = yes
		hide_window = no
		immediate = {
		random_country = {
		limit = { is_same_value = event_target:pirate_country }
		change_variable = {
		which = pirate_fleet
		value = 1
		}
		}
		}
		}	

#country_event = {	#waking up pirates BROKEN
#		id = pirates.502
#		is_triggered_only = yes
#		hide_window = yes
#		
#		immediate = {
#		random_country = {
#		limit = { is_same_value = event_target:pirate_country }
#		set_country_type = pirate_awakened
#		}
#		}
#		} 
		

								
country_event = {	#spawning pirate ships
	id = pirates.504
	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
	has_global_flag = pirates_attacking
	check_variable = {
		which = pirate_fleet
		value > 1 }
		}
		
	immediate = {
		random_country = {
		limit = { is_same_value = event_target:pirate_country }
	random_owned_fleet = {
	limit = { has_fleet_flag = pirate_idle }
	spawn_pirate_raiders = yes
	}
	change_variable = {
	which = pirate_fleet
	value = -1
	}
	}
	
	}
	}
	
country_event = {			#spawning pirate fleets
	id = pirates.505
	hide_window = yes
	is_triggered_only = yes
	fire_only_once = yes
	immediate = {
		random_country = {
		limit = { is_same_value = event_target:pirate_country }
	random_planet = {
	limit = { is_same_value = event_target:pirate_home }
	set_global_flag = pirates_attacking
	while = {
	count = 4
						create_fleet = {
						
						effect = {
							set_owner = event_target:pirate_country
							create_ship = {
								name = random
								design = "NAME_Swarm_Small"
								graphical_culture = "swarm_01"
									}
							log = "create_fleet log"
							assign_leader = random
							set_fleet_flag = pirate_fleet
							set_fleet_flag = pirate_idle
							set_location = {
								target = event_target:pirate_home
								distance = 35
								angle = random 
											}
							set_fleet_stance = aggressive
							set_aggro_range = 1000
							set_aggro_range_measure_from = self
							
							}
						}
					}
					
				}
			}
		}
	}
	
#country_event = {	#removing aggressive pirates flag
#	id = pirates.506
#	hide_window = no
#	is_triggered_only = yes
#	
#	immediate = {
#	remove_global_flag = pirates_attacking
#	}
#	}

fleet_event = {	#pirates move to hostage planet
	id = pirates.601
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
	log = "pirates.601 scope effect"
	print_scope_effect = yes
		This = {	
		set_fleet_flag = hostage_fleet
		remove_fleet_flag = pirate_idle
							queue_actions = {
								
										find_random_system = {
											trigger = {
												id = "pirate.600.trigger.1"
												NOT = { 
													has_star_flag = hostile_system 
													
													is_same_value = this.owner.capital_scope.solar_system
												}
												any_planet = {
													OR = {
													is_colony = yes 
													}
													exists = leader
													exists = controller
													#controller = { is_same_value = event_target:hostage_target }
												}
												#distance = {
												##	source = PREV
												#	max_distance = 100
												#	min_distance = 20
												#}
											}
											found_system = {
												move_to = THIS
												
											}	
										}
										find_closest_planet = {
											trigger = {
												id = "pirate.600.trigger.2"
												OR = {
													is_colony = yes 
												}
												exists = leader
											}
											found_planet = {
											
												orbit_planet = THIS
												wait = {
													duration = 80
												}
											}
										}

									}
								}
							}
			}
	

fleet_event = { #pirates go home
	id = pirates.602
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		This = {
			queue_actions = {
				move_to = event_target:pirate_home
				}
				set_fleet_flag = pirate_idle
				remove_fleet_flag = hostage_fleet
				remove_fleet_flag = orbital_raiders
			}
		}
	}
# Planet has reached 0 health
# This = Planet			# pirates take hostage
# From = Bombarder
planet_event = {
	id = pirates.603
	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
	from = { is_pirate = yes }
	}
	
	immediate = {
			This.owner = { country_event = { id = pirates.604 } }
			This = { leader = { save_event_target_as = hostage } 
					save_event_target_as = hostage_planet }
			solar_system = {
			random_fleet_in_system = {
			limit = {
				owner = { is_same_value = event_target:pirate_country } }
				Fleet_event = { id = pirates.602 } 
				}
			}	
		}
	}
		
country_event = {	# first ransom demand
	id = pirates.604
	title = GCW_pirates_event.604.name
	desc = GCW_pirates_event.604.desc
	picture = GFX_evt_pirate_hostage
	is_triggered_only = yes
	title = pirates.1.name
	desc = pirates.1.desc


	option = {
		name = pirates.1.a
		hidden_effect = {
		root = { add_energy = -3000 }
		change_variable = {
			which = pirate_fleet
			value = 5
				}

			}	
		}
	option = {
		name = pirates.1.b
		hidden_effect = {
		from = { 	
					leader = { 	exile_leader_as = hostage } }
					root = { country_event = { id = pirates.605 days = 30 random = 30 } }
				
					}
				}		 
			

	option = {
		name = pirates.1.c
		hidden_effect = {
	from = { leader = { kill_leader = { show_notification = yes } } }
					}
				}		 
			}
		
country_event = {	# second ransom demand
	id = pirates.605
	title = GCW_pirates_event.605.name
	desc = GCW_pirates_event.605.desc
	picture = GFX_evt_pirate_hostage
	is_triggered_only = yes
	title = pirates.2.name
	desc = pirates.2.desc
	

	option = {
		name = pirates.2.a
		hidden_effect = {
		root = { add_energy = -3000 }
		change_variable = {
			which = pirate_fleet
			value = 5
				}
		create_fleet = {
			name = "NAME_Other_Science_Ship"
			effect = {
				set_owner = root
				create_ship_design = {
					design = "NAME_Prototype"
					ftl = root
				}
				create_ship = {
					name = "NAME_From_Beyond"
					design = last_created_design
					graphical_culture = "extra_dimensional_01"
					prefix = no
					upgradable = no
				}
				set_location = from.capital_scope
				save_event_target_as = deleted_ship
			}
		}
		event_target:deleted_ship = {
			set_leader = hostage
			leader = {unassign_leader = this}
			delete_fleet = this
			destroy_ship = this
				}

			}
		}	
		
	
	option = {
		name = pirates.2.b
		hidden_effect = {
		event_target:hostage = { kill_leader = { show_notification = yes } }
				}		 
			}
	}
	
	

	

fleet_event = {	#pirates move to raiding system
	id = pirates.700
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
	log = "pirates.700 scope effect"
	print_scope_effect = yes
		This = {
			set_fleet_flag = orbital_raiders
			remove_fleet_flag = pirate_idle
							queue_actions = {
								
										find_random_system = {
											trigger = {
												id = "pirate.700.trigger.1"
												NOT = { 
													has_star_flag = hostile_system 
													is_same_value = this.owner.capital_scope.solar_system
												}
												any_planet = {
													NOT = {
													is_colony = yes 
													}
													exists = controller
													OR = {
													has_mining_station = yes
													has_research_station = yes	
												#controller = { is_same_value = event_target:hostage_target }
												}
												}
												#distance = {
												##	source = PREV
												#	max_distance = 100
												#	min_distance = 20
												#}
											}
											found_system = {
												move_to = THIS
											}	
										}
										find_closest_planet = {
											trigger = {
												id = "pirate.700.trigger.2"
												OR = {
													has_mining_station = yes
													has_research_station = yes	
												}
											}
											found_planet = {
												orbit_planet = THIS
												
											}
										}

									}
									fleet_event = { id = pirates.602 days = 90 }
								}
							}
			}
	

	
			

						