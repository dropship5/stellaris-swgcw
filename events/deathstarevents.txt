######################################
#
#	Death Star Events aka "That's no moon!"
#
#######################################

namespace = deathstar

fleet_event = {

	id = deathstar.1
	picture = GFX_evt_deathstar_firing
	is_triggered_only = yes
	hide_window = no
	title = deathstar.1.name
	desc = deathstar.1.desc
	

				
	trigger = {
		
		
		#log = "deathstar triggers"
		#print_scope_trigger = yes
		
		AND = {
				this = { is_ship_size = superweapon }
				from = { is_star = no }
				from = { is_asteroid = no }
				exists = orbit
				}
			}
		
	
	
	option = {
		name = deathstar.1.a
		hidden_effect = {
			FROM = {
				planet_event = { id = deathstar.2 }
				}
			set_global_flag = deathstar_used
			}
		}
	option = {
		name = deathstar.1.b
	}
	
	
#	option = {
#		name = deathstar.1.c
#		if = {				##### This is where you can use the deathstar for something other than blowing crap up :D
#		limit = {
#			has_global_flag = deathstar_used }
#			hidden_effect = {
#			from = { 
#			add_modifier = {
#			modifier = major_terrorist_action #will replace this bit with something that makes more sense later
#			days = 90  }
#					}			
#				}
#			}
#		}
	}
				

		

planet_event = {
	id = deathstar.2
	is_triggered_only = yes
	hide_window = yes
					
	immediate = {
	#log = "Deathstar 3rd scope effect"
	#print_scope_effect = yes
	this = { save_event_target_as = doomed_planet }
	create_ambient_object = {
			type = "explosion_particle_object"
			location = Root
		}
	#event_target:doomed_planet = { remove_planet = yes }
	#event_target:doomed_planet = { 
	#				change_pc = pc_planet_remnant 
	#				set_name = "[Root.GetName] Remnant"				
	#}
	Solar_system = {
		event_target:doomed_planet = { remove_planet = yes }
		spawn_planet = {
			class = "pc_asteroid"
			location = Root
			orbit_location = no
			orbit_angle_offset = 0
			orbit_distance_offset = 3
			size = 1
			init_effect = {
			#log = "1st spawn"
			#print_scope_effect = yes
			set_name = "[Root.GetName] Remnant"
			}
		}
		spawn_planet = {
			class = "pc_asteroid"
			location = Root
			orbit_location = no
			orbit_angle_offset = 1
			orbit_distance_offset = 2
			size = 1
			init_effect = {
			#log = "2nd spawn"
			#print_scope_effect = yes
			set_name = "[Root.GetName] Remnant 2"
			}
		}
		spawn_planet = {
			class = "pc_asteroid"
			location = Root
			orbit_location = no
			orbit_angle_offset = 2
			orbit_distance_offset = 0
			size = 1
			init_effect = {
			#log = "3rd spawn"
			#print_scope_effect = yes
			set_name = "[Root.GetName] Remnant 3"
			}
		}	
		
		spawn_planet = {
			class = "pc_asteroid"
			location = Root
			orbit_location = no
			orbit_angle_offset = -2
			orbit_distance_offset = 1
			size = 1
			init_effect = {
			#log = "4th spawn"
			#print_scope_effect = yes
			set_name = "[Root.GetName] Remnant 4"
			} }
		create_ambient_object = {
			type = "large_debris_object"
			location = Root
			use_3d_location = yes
			#entity_offset = { min = 10 max = 100 }
			#entity_offset_angle = { min = 90 max = 270 }
			entity_offset_height = 0


		}
		}
		}
		}
	
	

	
	