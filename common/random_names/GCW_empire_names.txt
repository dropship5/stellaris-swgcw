empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = galactic_empire
		}
	}
	format = "Galactic Empire"
	noun = "Imperial"
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = republic_loyalists
		}
	}
	format = "Republic Loyalists"
	noun = "Loyalist"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = rebel_alliance
		}
	}
	format = "Alliance to Restore the Republic"
	noun = "Rebel"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = hutt_cartel
		}
	}
	format = "Hutt Cartel"
	noun = "Hutt"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = mandalorian_sector
		}
	}
	format = "Mandalorian Sector"
	noun = "Mandalorian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = corporate_sector_authority
		}
	}
	format = "Corporate Sector Authority"
	noun = "Human"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = corellian_sector
		}
	}
	format = "Corellian Sector"
	noun = "Corellian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = hapes_consortium
		}
	}
	format = "Hapes Consortium"
	noun = "Hapan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = bothan_council
		}
	}
	format = "Bothan Council"
	noun = "Bothan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = confederate_remnant
		}
	}
	format = "Confederate Remnant"
	noun = "Confederate"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = the_centrality
		}
	}
	format = "The Centrality"
	noun = "Human"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = greater_javin
		}
	}
	format = "Greater Javin"
	noun = "Javin"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = juvex_sector
		}
	}
	format = "Juvex Sector"
	noun = "Juvex"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = senex_sector
		}
	}
	format = "Senex Sector"
	noun = "Senex"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = echani_domain
		}
	}
	format = "Echani Domain"
	noun = "Eshan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = trade_federation
		}
	}
	format = "Trade Federation"
	noun = "Member"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = bakuran_senate
		}
	}
	format = "Bakuran Senate"
	noun = "Bakuran"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = tion_hegemony
		}
	}
	format = "Tion Hegemony"
	noun = "Tion"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = zygerrian_slave_guild
		}
	}
	format = "Zygerrian Slaver Guild"
	noun = "Zygerrian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = banking_clan
		}
	}
	format = "Intergalactic Banking Clan"
	noun = "Clan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = kingdom_of_alderaan
		}
	}
	format = "Kingdom of Alderaan"
	noun = "Alderaanian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = principality_of_arkania
		}
	}
	format = "Principality of Arkania"
	noun = "Arkanian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = cathar
		}
	}
	format = "Cathar"
	noun = "Cathar"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = taris_authority
		}
	}
	format = "Taris Authority"
	noun = "Tarisan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = rodian_clans
		}
	}
	format = "Rodian Clans"
	noun = "Rodian"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = zabrak_high_council
		}
	}
	format = "Zabrak High Council"
	noun = "Zabrak"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = togrutan_dominion
		}
	}
	format = "Togrutan Dominion"
	noun = "Togruta"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = kastolar_sector
		}
	}
	format = "Kastolar Sector"
	noun = "Human"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = trandoshan_dominion
		}
	}
	format = "Trandoshan Dominion"
	noun = "Trandoshan"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = ord_mantell
		}
	}
	format = "Ord Mantell"
	noun = "Mantell"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = gaulus_sector
		}
	}
	format = "Gaulus Sector"
	noun = "Ryloth"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = duchy_of_zeltros
		}
	}
	format = "Duchy of Zeltros"
	noun = "Zeltron"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = kingdom_of_onderon
		}
	}
	format = "Kingdom of Onderon"
	noun = "Onderon"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = balawai_rep_council
		}
	}
	format = "Balawai Representative Council"
	noun = "Balawai"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = deadalis_sector
		}
	}
	format = "Deadalis Sector"
	noun = "Deadalis"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = utapauan_committee
		}
	}
	format = "Utapauan Committee"
	noun = "Utapau"	
}

empire_name_format = {
	random_weight = {
		factor = 0
		modifier = {
			add = 10000
			has_country_flag = pantoran_assembly
		}
	}
	format = "Pantoran Assembly"
	noun = "Pantora"	
}


