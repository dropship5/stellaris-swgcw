##################
### TECH COSTS ###
##################
#If you change any of these, use replace in files so the values are the same across all files

@tier1cost1 = 360
@tier1cost2 = 480
@tier1cost3 = 600
@tier1cost4 = 720

@tier2cost1 = 1000
@tier2cost2 = 1400
@tier2cost3 = 1800
@tier2cost4 = 2200

@tier3cost1 = 3000
@tier3cost2 = 4000
@tier3cost3 = 5000
@tier3cost4 = 6000

@tier4cost1 = 10000

####################
### TECH WEIGHTS ###
####################

@tier1weight1 = 100
@tier1weight2 = 95
@tier1weight3 = 90
@tier1weight4 = 85

@tier2weight1 = 75
@tier2weight2 = 70
@tier2weight3 = 65
@tier2weight4 = 60

@tier3weight1 = 50
@tier3weight2 = 45
@tier3weight3 = 40
@tier3weight4 = 35

@tier4weight1 = 30

# ##################
# Spaceport slots and Ship Sizes
# ##################

tech_cruiser_yard = {
	cost = @tier2cost1
	area = engineering
	tier = 2
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_5" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_battleship_yard = {
	cost = @tier2cost4
	area = engineering
	tier = 2
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_6" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

# ##################
# Strike Craft
# ##################

tech_strike_craft_1 = {
	cost = @tier2cost1
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_5" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_strike_craft_2 = {
	cost = @tier2cost2
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_strike_craft_1" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_strike_craft_3 = {
	cost = @tier2cost4
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_strike_craft_2" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}


# ##################
# Armies & Attachments
# ##################

# ##################
# Ship Armor Components
# ##################

#Nanocomposite Materials
tech_ship_armor_1 = {
	area = engineering
	cost = @tier1cost2
	tier = 1
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_spaceport_1" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

#Ceramo-Metal Materials
tech_ship_armor_2 = {
	area = engineering
	cost = @tier1cost4
	tier = 1
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_ship_armor_1" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

#Plasteel Materials
tech_ship_armor_3 = {
	area = engineering
	cost = @tier2cost2
	tier = 2
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_ship_armor_2" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

#Durasteel Materials
tech_ship_armor_4 = {
	area = engineering
	cost = @tier2cost4
	tier = 2
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_ship_armor_3" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

#Neutronium Materials
tech_ship_armor_5 = {
	area = engineering
	cost = @tier3cost3
	tier = 3
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_ship_armor_4" "tech_mine_neutronium" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

# ##################
# Ship Hull Improvement Components
# ##################

#Crystal-Infused Plating
tech_crystal_armor_1 = {
	area = engineering
	cost = @tier2cost2
	tier = 1
	category = { materials }
	ai_update_type = all	
	is_reverse_engineerable = no
	weight = 0
	weight_modifier = {
		modifier = {
			add = 0
			has_country_flag = crystal_armor_1_weight
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 0
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

#Crystal-Forged Plating
tech_crystal_armor_2 = {
	area = engineering
	cost = @tier3cost2
	tier = 2
	is_rare = yes
	category = { materials }
	ai_update_type = all	
	weight = 0
	
	ai_weight = {
		modifier = {
			factor = 0
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

# ##################
# Thruster Components
# ##################

#Chemical Thrusters
tech_thrusters_1 = {
	area = engineering
	cost = 0
	start_tech = yes
	tier = 0
	category = { rocketry }
	ai_update_type = all
	is_reverse_engineerable = no
	weight = 0
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_THRUSTER_1_TITLE"
			desc = "TECH_UNLOCK_THRUSTER_1_DESC"
		}
	}
}

#Ion Thrusters
tech_thrusters_2 = {
	area = engineering
	cost = @tier1cost1
	tier = 1
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_thrusters_1" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_2
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_3
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_THRUSTER_2_TITLE"
			desc = "TECH_UNLOCK_THRUSTER_2_DESC"
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

#Plasma Thrusters
tech_thrusters_3 = {
	area = engineering
	cost = @tier2cost1
	tier = 2
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_thrusters_2" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_4
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_5
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_THRUSTER_3_TITLE"
			desc = "TECH_UNLOCK_THRUSTER_3_DESC"
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

#Impulse Thrusters
tech_thrusters_4 = {
	area = engineering
	cost = @tier3cost1
	tier = 3
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_thrusters_3" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_6
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_THRUSTER_4_TITLE"
			desc = "TECH_UNLOCK_THRUSTER_4_DESC"
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

# ##################
# Defense Stations
# ##################

#Defense Platform
tech_space_defense_station_1 = {
	cost = 0
	tier = 0
	area = engineering
	category = { voidcraft }	
	prerequisites = { "tech_space_construction" }
	is_reverse_engineerable = no
	weight = 0
}

#Defense Station
tech_space_defense_station_2 = {
	cost = @tier2cost1
	area = engineering
	tier = 2
	category = { voidcraft }	
	prerequisites = { "tech_space_defense_station_1" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 2
			is_ai = yes
		}		
		modifier = {
			factor = 5
			has_technology = tech_spaceport_4
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_DEFENSE_STATION_TITLE"
			desc = "TECH_UNLOCK_DEFENSE_STATION_DESC"
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
		
	## unlock defense station
}

#Fortress
tech_space_defense_station_3 = {
	cost = @tier3cost1
	area = engineering
	tier = 3
	category = { voidcraft }	
	prerequisites = { "tech_space_defense_station_2" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 5
			has_technology = tech_spaceport_6
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_FORTRESS_TITLE"
			desc = "TECH_UNLOCK_FORTRESS_DESC"
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
		
	## unlock space fortress
}

# ##################
# Industry and Robotics
# ##################

#Engineering Lab 1
tech_engineering_lab_1 = {
	cost = @tier1cost1
	area = engineering
	tier = 1
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 5
			is_ai = yes
		}
		modifier = {
			factor = 2
			years_passed > 5
		}
		modifier = {
			factor = 2
			years_passed > 10
		}		
		modifier = {
			factor = 2
			years_passed > 15
		}		
		modifier = {
			factor = 2
			years_passed > 20
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

#Engineering Lab 2
tech_engineering_lab_2 = {
	cost = @tier2cost2
	area = engineering
	tier = 2
	category = { industry }	
	prerequisites = { "tech_engineering_lab_1" "tech_colonial_centralization" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 25
		}
		modifier = {
			factor = 2
			years_passed > 30
		}		
		modifier = {
			factor = 2
			years_passed > 35
		}		
		modifier = {
			factor = 2
			years_passed > 40
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

#Engineering Lab 3
tech_engineering_lab_3 = {
	cost = @tier3cost3
	area = engineering
	tier = 3
	category = { industry }	
	prerequisites = { "tech_engineering_lab_2" "tech_galactic_administration" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 25
		}
		modifier = {
			factor = 2
			years_passed > 30
		}		
		modifier = {
			factor = 2
			years_passed > 35
		}		
		modifier = {
			factor = 2
			years_passed > 40
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_robotic_workers = {
	cost = @tier1cost3
	area = engineering
	tier = 1
	category = { industry }	
	prerequisites = { "tech_powered_exoskeletons" }
	is_reverse_engineerable = no
	weight = 0
	
	gateway = robotics
	
	weight_modifier = {
		factor = 1.5
		modifier = {
			factor = 0
			OR = {
				has_ethic = "ethic_spiritualist"
				has_ethic = "ethic_fanatic_spiritualist"
				has_ethic = "ethic_hive_mind"
			}	
		}			
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			NOT = { has_ai_personality_behaviour = robot_exploiter }
			NOT = { has_ai_personality_behaviour = robot_liberator }
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_droid_workers = {
	cost = @tier2cost2
	area = engineering
	tier = 2
	category = { industry }
	prerequisites = { "tech_robotic_workers" "tech_colonial_centralization" }
	is_reverse_engineerable = no
	weight = 0
	
	gateway = robotics

	weight_modifier = {
		modifier = {
			factor = 0
			OR = {
				has_ethic = "ethic_spiritualist"
				has_ethic = "ethic_fanatic_spiritualist"
				has_ethic = "ethic_hive_mind"
			}	
		}
		modifier = {
			factor = 2
			has_valid_civic = civic_mechanists
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
				has_level > 2
			}
		}
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			NOT = { has_ai_personality_behaviour = robot_exploiter }
			NOT = { has_ai_personality_behaviour = robot_liberator }
		}
		modifier = {
			factor = 2
			has_valid_civic = civic_mechanists
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_synthetic_workers = {
	cost = @tier3cost2
	area = engineering
	tier = 3
	category = { industry }
	prerequisites = { "tech_droid_workers" "tech_galactic_administration" "tech_sentient_ai" }
	is_reverse_engineerable = no
	is_rare = yes
	is_dangerous = yes
	weight = 0

	## unlock robot POP lvl 3, robot Army lvl 2
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			OR = {
				has_ethic = "ethic_spiritualist"
				has_ethic = "ethic_fanatic_spiritualist"
				has_ethic = "ethic_hive_mind"
			}	
		}
		modifier = {
			factor = 2
			has_valid_civic = civic_mechanists
		}
		modifier = {
			factor = 4
			has_ascension_perk = ap_the_flesh_is_weak
		}
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = engineering
					has_trait = "leader_trait_maniacal"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_spark_of_genius"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_expertise_industry"
				}
			}
		}
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			NOT = { has_ai_personality_behaviour = robot_exploiter }
			NOT = { has_ai_personality_behaviour = robot_liberator }
		}
		modifier = {
			factor = 2
			has_valid_civic = civic_mechanists
		}
		modifier = {
			factor = 2
			has_ascension_perk = ap_the_flesh_is_weak
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

#Synthetic Personality Matrix
tech_synthetic_leaders = {
	cost = @tier3cost3
	area = engineering
	tier = 3
	category = { industry }
	prerequisites = { "tech_synthetic_workers" }
	is_reverse_engineerable = no
	weight = 0
	
	feature_flags = { robot_leaders }
		
	## unlocks robot-leaders
	
	weight_modifier = {
		modifier = {
			factor = 0
			OR = {
				has_ethic = "ethic_spiritualist"
				has_ethic = "ethic_fanatic_spiritualist"
				has_ethic = "ethic_hive_mind"
			}	
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
				has_level > 3
			}
		}
	}

	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			NOT = { has_ai_personality_behaviour = robot_exploiter }
			NOT = { has_ai_personality_behaviour = robot_liberator }
		}
	}
}


# ##################
# Space Mining
# ##################

# ##################
# Afterburners
# ##################

#Afterburners
tech_afterburners_1 = {
	area = engineering
	cost = @tier2cost1
	tier = 2
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_thrusters_2" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_thrusters_2
		}
		modifier = {
			factor = 2
			has_technology = tech_thrusters_3
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_AFTERBURNER_1_TITLE"
			desc = "TECH_UNLOCK_AFTERBURNER_1_DESC"
		}
	}
	
	ai_weight = {
		factor = 2 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

#Advanced Afterburners
tech_afterburners_2 = {
	area = engineering
	cost = @tier3cost1
	tier = 3
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_afterburners_1" "tech_thrusters_3" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_AFTERBURNER_2_TITLE"
			desc = "TECH_UNLOCK_AFTERBURNER_2_DESC"
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

# ##################
# Building Speed
# ##################

# ##################
# Megastructures
# ##################