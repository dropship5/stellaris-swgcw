### PRE-SCRIPTED

part = {
	location = 0
	localization = "START_SCREEN_GE"
	
	trigger = {
		has_country_flag = galactic_empire
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_RL"
	
	trigger = {
		has_country_flag = republic_loyalists
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_HC"
	
	trigger = {
		has_country_flag = hutt_cartel
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_MAN"
	
	trigger = {
		has_country_flag = mandalorian_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_CSA"
	
	trigger = {
		has_country_flag = corporate_sector_authority
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_COR"
	
	trigger = {
		has_country_flag = corellian_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_HAP"
	
	trigger = {
		has_country_flag = hapes_consortium
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_BOT"
	
	trigger = {
		has_country_flag = bothan_council
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_CIS"
	
	trigger = {
		has_country_flag = confederate_remnant
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_CEN"
	
	trigger = {
		has_country_flag = the_centrality
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_JAV"
	
	trigger = {
		has_country_flag = greater_javin
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_JUV"
	
	trigger = {
		has_country_flag = juvex_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_SEN"
	
	trigger = {
		has_country_flag = senex_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ECH"
	
	trigger = {
		has_country_flag = echani_domain
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_TRF"
	
	trigger = {
		has_country_flag = trade_federation
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_BAK"
	
	trigger = {
		has_country_flag = bakuran_senate
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_TIO"
	
	trigger = {
		has_country_flag = tion_hegemony
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ZYG"
	
	trigger = {
		has_country_flag = zygerrian_slave_guild
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_IBC"
	
	trigger = {
		has_country_flag = banking_clan
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_KOA"
	
	trigger = {
		has_country_flag = kingdom_of_alderaan
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_POA"
	
	trigger = {
		has_country_flag = principality_of_arkania
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_CAT"
	
	trigger = {
		has_country_flag = cathar
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_TAR"
	
	trigger = {
		has_country_flag = taris_authority
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ROD"
	
	trigger = {
		has_country_flag = rodian_clans
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ZHC"
	
	trigger = {
		has_country_flag = zabrak_high_council
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_TOG"
	
	trigger = {
		has_country_flag = togrutan_dominion
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_KAS"
	
	trigger = {
		has_country_flag = kastolar_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_TRA"
	
	trigger = {
		has_country_flag = trandoshan_dominion
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ORD"
	
	trigger = {
		has_country_flag = ord_mantell
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_GAU"
	
	trigger = {
		has_country_flag = gaulus_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_ZEL"
	
	trigger = {
		has_country_flag = duchy_of_zeltros
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_OND"
	
	trigger = {
		has_country_flag = kingdom_of_onderon
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_BRC"
	
	trigger = {
		has_country_flag = balawai_rep_council
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_DOR"
	
	trigger = {
		has_country_flag = deadalis_sector
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_UTA"
	
	trigger = {
		has_country_flag = utapauan_committee
	}		
}
part = {
	location = 0
	localization = "START_SCREEN_PAN"
	
	trigger = {
		has_country_flag = pantoran_assembly
	}		
}