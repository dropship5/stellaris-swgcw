@b0time = 120
@b0cost = 30
@b0effect = 1
@b0upkeep = 0.5

@b1time = 210
@b1cost = 60
@b1effect = 2
@b1upkeep = 1

@b2time = 180
@b2cost = 90
@b2effect = 3
@b2upkeep = 1.5

@b3time = 180
@b3cost = 120
@b3effect = 4
@b3upkeep = 2

@b4time = 180
@b4cost = 150
@b4effect = 5
@b4upkeep = 2.5

@b5time = 180
@b5cost = 180
@b5effect = 8
@b5upkeep = 3

@science1 = 1
@science2 = 2
@science3 = 3
@science4 = 4
@science5 = 6

@natural_equilibrium_happiness = 0.02

#
#	ENGINEERING
#

building_engineering_facility_1 = {
	base_buildtime = @b2time
	is_listed = no
	
	cost = {
		minerals = @b2cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_1"
			planet = {
				OR = {
					has_building = "building_capital_1"
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science2
		physics_research = @science1
		society_research = @science1
	}
	
	required_resources = {
		energy = @b2upkeep
	}
	
	upgrades = {
		building_engineering_facility_2
	}	
	
	prerequisites = {
		"tech_engineering_lab_1"
	}
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}		
}

building_engineering_facility_2 = {
	base_buildtime = @b3time
	is_listed = no
	
	cost = {
		minerals = @b3cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science3
		physics_research = @science1
		society_research = @science1
	}
	
	required_resources = {
		energy = @b3upkeep
	}
	
	upgrades = {
		building_engineering_facility_3
	}	
	
	prerequisites = {
		"tech_engineering_lab_2"
	}

	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no		
	}		
}

building_engineering_facility_3 = {
	base_buildtime = @b4time
	is_listed = no
	
	cost = {
		minerals = @b4cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science4
		physics_research = @science1
		society_research = @science1
	}
	
	required_resources = {
		energy = @b4upkeep
	}
	
	upgrades = {
		building_engineering_facility_4
	}	
	
	prerequisites = {
		"tech_engineering_lab_3"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}		
}

building_engineering_facility_4 = {
	base_buildtime = @b5time
	is_listed = no
	
	cost = {
		minerals = @b5cost
	}
	
	potential = {
		planet = {
			is_capital = yes
			NOT = { is_planet_class = pc_habitat }	
		}
	}
	
	destroy_if = {
		planet = { is_capital = no }
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_3"
			planet = {
				has_building = "building_capital_3"
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science5
		physics_research = @science1
		society_research = @science1
	}
	
	required_resources = {
		energy = @b5upkeep
	}
	
	prerequisites = {
		"tech_engineering_lab_3"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no
	}		
}

#
#	PHYSICS
#

building_physics_lab_1 = {
	base_buildtime = @b2time
	is_listed = no
	
	cost = {
		minerals = @b2cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_1"
			planet = {
				OR = {
					has_building = "building_capital_1"
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science2
		society_research = @science1
	}
	
	required_resources = {
		energy = @b2upkeep
	}
	
	upgrades = {
		building_physics_lab_2
	}	
	
	prerequisites = {
		"tech_physics_lab_1"
	}

	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no
	}		
}

building_physics_lab_2 = {
	base_buildtime = @b3time
	is_listed = no
	
	cost = {
		minerals = @b3cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science3
		society_research = @science1
	}
	
	required_resources = {
		energy = @b3upkeep
	}
	
	upgrades = {
		building_physics_lab_3
	}
	
	prerequisites = {
		"tech_physics_lab_2"
	}
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no
	}	
}

building_physics_lab_3 = {
	base_buildtime = @b4time
	is_listed = no
	
	cost = {
		minerals = @b4cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science4
		society_research = @science1
	}
	
	required_resources = {
		energy = @b4upkeep
	}
	
	upgrades = {
		building_physics_lab_4
	}
	
	prerequisites = {
		"tech_physics_lab_3"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no
	}	
}

building_physics_lab_4 = {
	base_buildtime = @b5time
	is_listed = no
	
	cost = {
		minerals = @b5cost
	}
	
	potential = {
		planet = {
			is_capital = yes
			NOT = { is_planet_class = pc_habitat }	
		}
	}
	
	destroy_if = {
		planet = { is_capital = no }
	}
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_3"
			planet = {
				has_building = "building_capital_3"
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science5
		society_research = @science1
	}
	
	required_resources = {
		energy = @b5upkeep
	}
	
	prerequisites = {
		"tech_physics_lab_3"
	}

	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no
	}		
}

#
#	SOCIETY
#

building_biolab_1 = {
	base_buildtime = @b2time
	is_listed = no
	
	cost = {
		minerals = @b2cost
	}
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_1"
			planet = {
				OR = {
					has_building = "building_capital_1"
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science1
		society_research = @science2
	}
	
	required_resources = {
		energy = @b2upkeep
	}
	
	upgrades = {
		building_biolab_2
	}
	
	prerequisites = {
		"tech_biolab_1"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}	
}

building_biolab_2 = {
	base_buildtime = @b3time
	is_listed = no
	
	cost = {
		minerals = @b3cost
	}	
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}

	produced_resources = {
		engineering_research = @science1
		physics_research = @science1
		society_research = @science3
	}
	
	required_resources = {
		energy = @b3upkeep
	}
	
	upgrades = {
		building_biolab_3
	}
	
	prerequisites = {
		"tech_biolab_2"
	}

	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}	
}

building_biolab_3 = {
	base_buildtime = @b4time
	is_listed = no
	
	cost = {
		minerals = @b4cost
	}	
	
	potential = {
		planet = {
			NOT = { is_planet_class = pc_habitat }	
		}
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_2"
			planet = {
				OR = {
					has_building = "building_capital_2"
					has_building = "building_capital_3"
				}
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science1
		society_research = @science4
	}
	
	required_resources = {
		energy = @b4upkeep
	}
	
	upgrades = {
		building_biolab_4
	}
	
	prerequisites = {
		"tech_biolab_3"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}	
}

building_biolab_4 = {
	base_buildtime = @b5time
	is_listed = no
	
	cost = {
		minerals = @b5cost
	}
	
	potential = {
		planet = {
			is_capital = yes
			NOT = { is_planet_class = pc_habitat }	
		}
	}
	
	destroy_if = {
		planet = { is_capital = no }
	}	
	
	allow = {
		custom_tooltip = {
			text = "requires_building_capital_3"
			planet = {
				has_building = "building_capital_3"
			}
		}
	}
	
	produced_resources = {
		engineering_research = @science1
		physics_research = @science1
		society_research = @science5
	}
	
	required_resources = {
		energy = @b5upkeep
	}
	
	prerequisites = {
		"tech_biolab_3"
	}	
	
	ai_allow = {
		# If planet only has slaves/robots, keep it to mining and farming...
		is_slave_tile_or_planet = no	
	}	
}