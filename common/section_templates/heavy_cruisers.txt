# Dreadnought class #

	ship_section_template = {	
		key = "hc_heavycruiser"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}		
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}

# Acclamator class #

	ship_section_template = {	
		key = "hc_torpedoboat"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_cruiser_mid2 }	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "TORPEDO_1"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_01"
		}
		component_slot = {
			name = "TORPEDO_2"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_02"
		}				
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Assault Frigate #

	ship_section_template = {
		key = "hc_gunship"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_cruiser_mid3 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}		
		component_slot = {
			name = "SMALL_GUN_05"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_05"
		}	
		component_slot = {
			name = "SMALL_GUN_06"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_06"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Victory I / MC60 #

	ship_section_template = {
		key = "hc_battlecarrier"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid4_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_cruiser_mid4 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}			
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}			
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Victory II #

	ship_section_template = {
		key = "hc_command_battlecarrier"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid5_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_cruiser_mid5 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}			
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}			
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Assault Frigate MK II #

	ship_section_template = {
		key = "hc_command_gunship"	
		ship_size = heavy_cruiser
		fits_on_slot = mid	
		entity = "heavy_cruiser_mid6_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_cruiser_mid6 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}		
		component_slot = {
			name = "SMALL_GUN_05"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_05"
		}	
		component_slot = {
			name = "SMALL_GUN_06"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_06"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}	