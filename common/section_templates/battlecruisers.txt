


# Allegiance-class SD #

	ship_section_template = {	
		key = "bc_superbattleship"	
		ship_size = battlecruiser
		fits_on_slot = mid	
		entity = "battlecruiser_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_battlecruiser_mid1 }	
		ai_weight = { factor = 20 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}
		component_slot = {
			name = "EXTRA_LARGE_03"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_03"
		}
		component_slot = {
			name = "EXTRA_LARGE_04"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Titan-class SD / MC90 #

	ship_section_template = {
		key = "bc_battlecarrier"	
		ship_size = battlecruiser
		fits_on_slot = mid	
		entity = "battlecruiser_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_battlecruiser_mid2 }
		ai_weight = { factor = 20 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}		
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}
		component_slot = {
			name = "STRIKE_CRAFT_06"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_06"
		}
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Recusant II-class / Procurator-class #

	ship_section_template = {	
		key = "bc_battleship"	
		ship_size = battlecruiser
		fits_on_slot = mid	
		entity = "battlecruiser_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_battlecruiser_mid3 }	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}
		component_slot = {
			name = "EXTRA_LARGE_03"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_03"
		}
		component_slot = {
			name = "EXTRA_LARGE_04"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Bulwark III-class #

	ship_section_template = {
		key = "bc_carrier"	
		ship_size = battlecruiser
		fits_on_slot = mid	
		entity = "battlecruiser_mid4_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_battlecruiser_mid4 }	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}
		component_slot = {
			name = "STRIKE_CRAFT_06"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_06"
		}
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Lucrehulk / Goliath #

	ship_section_template = {
		key = "bc_supercarrier"	
		ship_size = battlecruiser
		fits_on_slot = mid	
		entity = "battlecruiser_mid5_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_battlecruiser_mid5 }	
		ai_weight = { factor = 10 }
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}
		component_slot = {
			name = "STRIKE_CRAFT_06"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_06"
		}
		component_slot = {
			name = "STRIKE_CRAFT_07"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_07"
		}
		component_slot = {
			name = "STRIKE_CRAFT_08"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_08"
		}
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}