


# Executor-class SSD / Mediator-class #

	ship_section_template = {	
		key = "super_dreadnought"	
		ship_size = dreadnought
		fits_on_slot = mid	
		entity = "dreadnought_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_dreadnought_mid1 }	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}
		component_slot = {
			name = "EXTRA_LARGE_03"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_03"
		}
		component_slot = {
			name = "EXTRA_LARGE_04"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_04"
		}	
		component_slot = {
			name = "EXTRA_LARGE_05"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_05"
		}
		component_slot = {
			name = "EXTRA_LARGE_06"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_06"
		}
		component_slot = {
			name = "EXTRA_LARGE_07"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_07"
		}
		component_slot = {
			name = "EXTRA_LARGE_08"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_08"
		}	
		component_slot = {
			name = "EXTRA_LARGE_09"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_09"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}
		component_slot = {
			name = "STRIKE_CRAFT_06"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_06"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# MandatorII-class Dreadnought #

	ship_section_template = {	
		key = "big_dreadnought"	
		ship_size = dreadnought
		fits_on_slot = mid	
		entity = "dreadnought_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_dreadnought_mid2 }	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}
		component_slot = {
			name = "EXTRA_LARGE_03"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_03"
		}
		component_slot = {
			name = "EXTRA_LARGE_04"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_04"
		}	
		component_slot = {
			name = "EXTRA_LARGE_05"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_05"
		}
		component_slot = {
			name = "EXTRA_LARGE_06"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_06"
		}
		component_slot = {
			name = "EXTRA_LARGE_07"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_07"
		}
		component_slot = {
			name = "EXTRA_LARGE_08"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_08"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Mandator-class Dreadnought #

	ship_section_template = {	
		key = "dreadnought"	
		ship_size = dreadnought
		fits_on_slot = mid	
		entity = "dreadnought_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_dreadnought_mid3 }	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "EXTRA_LARGE_01"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}
		component_slot = {
			name = "EXTRA_LARGE_02"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_02"
		}
		component_slot = {
			name = "EXTRA_LARGE_03"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_03"
		}
		component_slot = {
			name = "EXTRA_LARGE_04"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_04"
		}	
		component_slot = {
			name = "EXTRA_LARGE_05"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_05"
		}
		component_slot = {
			name = "EXTRA_LARGE_06"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_06"
		}
		component_slot = {
			name = "EXTRA_LARGE_07"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_07"
		}	
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}