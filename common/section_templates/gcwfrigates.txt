# FRIGATE / ADV FRIGATE SECTIONS / INTERDICTOR SECTIONS #

# Nebulon B #

	ship_section_template = {	
		key = "fr_battlecarrier"	
		ship_size = gcwfrigate
		fits_on_slot = mid	
		entity = "gcwfrigate_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 15 }	
			
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Eidolon-class / Quasar-class / Pinnance-class #

	ship_section_template = {	
		key = "fr_carrier"	
		ship_size = gcwfrigate
		fits_on_slot = mid	
		entity = "gcwfrigate_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}		
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}

# Bayonet-class / CC-9600 / Vengeance-class / Trafedll-class #

	ship_section_template = {	
		key = "fr_gunship"	
		ship_size = gcwfrigate
		fits_on_slot = mid	
		entity = "gcwfrigate_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
			
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
	
	
### ADV FRIGATES ###	

# Nebulon B2 #

	ship_section_template = {	
		key = "af_battlecarrier"	
		ship_size = adv_frigate
		fits_on_slot = mid	
		entity = "adv_frigate_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 180	
	}
	
# Lancer-class #

	ship_section_template = {	
		key = "af_pointdefense"	
		ship_size = adv_frigate
		fits_on_slot = mid	
		entity = "adv_frigate_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_adv_frigate_mid2 }	
		ai_weight = { factor = 15 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}		
		component_slot = {
			name = "PD_05"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_05"
		}	
		component_slot = {
			name = "PD_06"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_06"
		}	
		component_slot = {
			name = "PD_07"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_07"
		}	
		component_slot = {
			name = "PD_08"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_08"
		}	
		component_slot = {
			name = "PD_09"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_09"
		}	
		component_slot = {
			name = "PD_010"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_10"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 180	
	}
	
# MC30c #

	ship_section_template = {	
		key = "af_torpedoboat"	
		ship_size = adv_frigate
		fits_on_slot = mid	
		entity = "adv_frigate_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_adv_frigate_mid3 }	
		ai_weight = { factor = 15 }	
			
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "TORPEDO_01"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_01"
		}	
		component_slot = {
			name = "TORPEDO_02"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_02"
		}	
		component_slot = {
			name = "TORPEDO_03"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_03"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 180	
	}
	
	
	
### INTERDICTORS ###	

# CC-7700 #

	ship_section_template = {	
		key = "in_gunship"	
		ship_size = interdictor
		fits_on_slot = mid	
		entity = "interdictor_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}