# STAR DESTROYER AND ADV STAR DESTROYER SECTIONS


# Imperial I-class / MC80 Libery-class #

	ship_section_template = {
		key = "sd_battlecarrier"	
		ship_size = star_destroyer
		fits_on_slot = mid	
		entity = "star_destroyer_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_star_destroyer_mid1 }		
		ai_weight = { factor = 20 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}
		component_slot = {
			name = "MEDIUM_GUN_07"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_07"
		}
		component_slot = {
			name = "MEDIUM_GUN_08"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_08"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}			
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}	
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
	
# MC80a #

	ship_section_template = {	
		key = "sd_supercarrier"	
		ship_size = star_destroyer
		fits_on_slot = mid	
		entity = "star_destroyer_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_star_destroyer_mid2 }	
		ai_weight = { factor = 15 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
# Venator/Providence #

	ship_section_template = {	
		key = "sd_carrier"	
		ship_size = star_destroyer
		fits_on_slot = mid	
		entity = "star_destroyer_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_star_destroyer_mid3 }	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}	
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
# Invincible/Recusant #

	ship_section_template = {	
		key = "sd_battleship"	
		ship_size = star_destroyer
		fits_on_slot = mid	
		entity = "star_destroyer_mid4_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}
		component_slot = {
			name = "MEDIUM_GUN_07"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_07"
		}
		component_slot = {
			name = "MEDIUM_GUN_08"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_08"
		}
		component_slot = {
			name = "MEDIUM_GUN_09"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_09"
		}
		component_slot = {
			name = "MEDIUM_GUN_010"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_10"
		}		
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
##### ADVANCED STAR DESTROYERS #####	
	
# Imperial II-class / Corellian Destroyer / Keldabe-class #

	ship_section_template = {	
		key = "asd_battlecarrier"	
		ship_size = adv_star_destroyer
		fits_on_slot = mid	
		entity = "adv_star_destroyer_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_adv_star_destroyer_mid1 }	
		ai_weight = { factor = 20 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}
		component_slot = {
			name = "MEDIUM_GUN_07"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_07"
		}
		component_slot = {
			name = "MEDIUM_GUN_08"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_08"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
# Home One-class #

	ship_section_template = {	
		key = "asd_supercarrier"	
		ship_size = adv_star_destroyer
		fits_on_slot = mid	
		entity = "adv_star_destroyer_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_adv_star_destroyer_mid2 }	
		ai_weight = { factor = 20 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_04"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_04"
		}		
		component_slot = {
			name = "STRIKE_CRAFT_05"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_05"
		}		
		component_slot = {
			name = "STRIKE_CRAFT_06"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_06"
		}		
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
# Tector-class SD / Dauntless-class #

	ship_section_template = {	
		key = "asd_battleship"	
		ship_size = adv_star_destroyer
		fits_on_slot = mid	
		entity = "adv_star_destroyer_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}
		component_slot = {
			name = "MEDIUM_GUN_07"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_07"
		}
		component_slot = {
			name = "MEDIUM_GUN_08"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_08"
		}	
		component_slot = {
			name = "MEDIUM_GUN_09"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_09"
		}	
		component_slot = {
			name = "XL_GUN_1"
			slot_size = extra_large
			slot_type = weapon
			locatorname = "xl_gun_01"
		}	
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}
	
# Interdictor-class SD #

	ship_section_template = {	
		key = "asd_interdictor"	
		ship_size = adv_star_destroyer
		fits_on_slot = mid	
		entity = "adv_star_destroyer_mid4_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_adv_star_destroyer_mid4 }	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "MEDIUM_GUN_03"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_03"
		}
		component_slot = {
			name = "MEDIUM_GUN_04"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_04"
		}	
		component_slot = {
			name = "MEDIUM_GUN_05"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_05"
		}
		component_slot = {
			name = "MEDIUM_GUN_06"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_06"
		}
		component_slot = {
			name = "MEDIUM_GUN_07"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_07"
		}
		component_slot = {
			name = "MEDIUM_GUN_08"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_08"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}	
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 640	
	}