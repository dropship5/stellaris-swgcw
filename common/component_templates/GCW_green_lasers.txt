# DEFAULT VALUES:
# is_missile = no
# is_beam = no
# is_point_defence = no

#####Blasters#####

weapon_component_template = {
	type = point_defence
	
	key = "BLASTER_G_1"
	size = point_defence
	entity = "invisible_turret_entity"
	
	icon = "GFX_ship_part_ship_blaster_g_1"
	icon_frame = 1
	
	prerequisites = { "tech_green_turbolaser_1" }
	component_set = "BLASTER_G_1"
	projectile_gfx = "blaster_s_g"
	tags = { weapon_type_point_defense }
	ai_tags = { weapon_role_point_defense }
	upgrades_to = "BLASTER_G_2"
}

#####Turbolasers######

weapon_component_template = {
	type = instant
	
	key = "TURBOLASER_G_1"
	size = small
	entity = "invisible_turret_entity"
	
	icon = "GFX_ship_part_turbo_g_1"
	icon_frame = 1
	
	prerequisites = { "tech_green_turbolaser_1" }
	component_set = "TURBOLASER_G_1"
	projectile_gfx = "turbolaser_m_g"
	tags = { weapon_type_energy }
	ai_tags = { weapon_role_anti_armor }
	upgrades_to = "TURBOLASER_G_2"
}

#####Heavy Turbolasers######

weapon_component_template = {
	type = instant
	
	key = "HEAVY_TURBOLASER_G_1"
	size = medium
	entity = "invisible_turret_entity"
	
	icon = "GFX_ship_part_heavy_turbo_g_1"
	icon_frame = 1
	
	prerequisites = { "tech_green_turbolaser_1" }
	component_set = "HEAVY_TURBOLASER_G_1"
	projectile_gfx = "turbolaser_h_g"
	tags = { weapon_type_energy }
	ai_tags = { weapon_role_anti_armor }
	upgrades_to = "HEAVY_TURBOLASER_G_2"
}

#####XL Turbolasers######

weapon_component_template = {
	type = instant
	
	key = "XL_TURBOLASER_G_1"
	size = extra_large
	entity = "invisible_turret_entity"
	
	icon = "GFX_ship_part_xl_turbo_g_1"
	icon_frame = 1
	
	prerequisites = { "tech_green_turbolaser_1" }
	component_set = "XL_TURBOLASER_G_1"
	projectile_gfx = "turbolaser_x_g"
	tags = { weapon_type_energy }
	ai_tags = { weapon_role_anti_armor }
	upgrades_to = "XL_TURBOLASER_G_2"
}
