utility_component_template = {
	key = "DEFAULT_COMPUTER_ALL"
	size = small
	icon = "GFX_ship_part_computer_default"
	icon_frame = 1
	power = 0
	cost = 0
	class_restriction = { shipclass_military }
	component_set = "GCW_combat_computers"
	ship_behavior = "battleship_default"
	upgrades_to = "DEFAULT_COMPUTER_ALL_2"
	
	ai_weight = {
		weight = 1
	}	
}