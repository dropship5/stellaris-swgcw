#############
#	POWER	#
#############

@power_CR1 = 20
@power_CR2 = 40
@power_CR3 = 60
@power_CR4 = 80
@power_CR5 = 100

#############
#	COST	#
#############

@cost_CR1 = 20
@cost_CR2 = 25
@cost_CR3 = 30
@cost_CR4 = 35
@cost_CR5 = 40

# CR Fusion Reactors

utility_component_template = {
	key = "SMALL_CR_FUSION_REACTOR"
	size = small
	icon = "GFX_ship_part_fusion_CR"
	icon_frame = 1
	power = @power_CR1
	cost = @cost_CR1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = -0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "CR_FUSION_REACTOR"
	upgrades_to = "SMALL_CR_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "MEDIUM_CR_FUSION_REACTOR"
	size = medium
	icon = "GFX_ship_part_fusion_CR"
	icon_frame = 1
	power = @power_CR1
	cost = @cost_CR1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = -0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "CR_FUSION_REACTOR"
	upgrades_to = "MEDIUM_CR_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "LARGE_CR_FUSION_REACTOR"
	size = large
	icon = "GFX_ship_part_fusion_CR"
	icon_frame = 1
	power = @power_CR1
	cost = @cost_CR1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = -0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "CR_FUSION_REACTOR"
	upgrades_to = "LARGE_CR_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "AUX_CR_FUSION_REACTOR"
	size = aux
	icon = "GFX_ship_part_fusion_CR"
	icon_frame = 1
	power = @power_CR1
	cost = @cost_CR1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = -0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "CR_FUSION_REACTOR"
	upgrades_to = "AUX_CR_COLD_FUSION_REACTOR"
}


