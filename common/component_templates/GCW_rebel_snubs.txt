@fighter_attack_range = 20
@bomber_attack_range = 20
@launch_time = 4
@fighter_speed = 2.4
@bomber_speed = 2.1
@count = 12
@regen = 0.15

##################
# Rebel
##################

strike_craft_component_template = {
	key = "V_WING"
	size = LARGE
	entity = "rebel_01_Z95_entity"
	
	weapon_type = point_defence
	projectile_gfx = "blaster_f_b"
	power = -20
	cost = 20
	
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 2 max = 4 }
	cooldown = 6
	range = @fighter_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 0
	shield = 100
	evasion = 0.5
	
	speed = @fighter_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0.5
	armor_penetration = 0
	
	ship_behavior = "fighters_behavior"
	
	icon = "GFX_ship_part_v_wing"
	icon_frame = 1
	component_set = "V_WING"
	prerequisites = { "tech_rebel_snub_1" }
	tags = { weapon_type_strike_craft }
	upgrades_to = "Z95_FIGHTER"
}

strike_craft_component_template = {
	key = "Z95_FIGHTER"
	size = LARGE
	entity = "rebel_01_Z95_entity"
	
	weapon_type = point_defence
	projectile_gfx = "blaster_f_r"
	power = -20
	cost = 20
	
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 3 max = 4 }
	cooldown = 6
	range = @fighter_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 0
	shield = 115
	evasion = 0.5
	
	speed = @fighter_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0.5
	armor_penetration = 0
	
	ship_behavior = "fighters_behavior"
	
	icon = "GFX_ship_part_z95"
	icon_frame = 1
	component_set = "Z95_FIGHTER"
	prerequisites = { "tech_rebel_snub_2" }
	tags = { weapon_type_strike_craft }
	upgrades_to = "A_WING"
}

strike_craft_component_template = {
	key = "A_WING"
	size = LARGE
	entity = "rebel_01_A_Wing_entity"
	
	weapon_type = point_defence
	projectile_gfx = "blaster_f_r"
	power = -1
	cost = 20
	
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 3 max = 5 }
	cooldown = 6
	range = @fighter_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 1
	shield = 130
	evasion = 0.5
	
	speed = @fighter_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0.5
	armor_penetration = 0
	
	ship_behavior = "fighters_behavior"
	
	icon = "GFX_ship_part_a_wing"
	icon_frame = 1
	component_set = "A_WING"
	prerequisites = { "tech_rebel_snub_3" }
	tags = { weapon_type_strike_craft }
	upgrades_to = "X_WING"
}

strike_craft_component_template = {
	key = "X_WING"
	size = LARGE
	entity = "rebel_01_X_Wing_entity"
	
	weapon_type = point_defence
	projectile_gfx = "blaster_f_r"
	power = -1
	cost = 20
	
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 4 max = 5 }
	cooldown = 6
	range = @fighter_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 1
	shield = 150
	evasion = 0.5
	
	speed = @fighter_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0.5
	armor_penetration = 0
	
	ship_behavior = "fighters_behavior"
	
	icon = "GFX_ship_part_x_wing"
	icon_frame = 1
	component_set = "X_WING"
	prerequisites = { "tech_rebel_snub_4" }
	tags = { weapon_type_strike_craft }
}

# Bomber

strike_craft_component_template = {
	key = "Y_WING"
	size = LARGE
	entity = "rebel_01_Y_Wing_entity"

	weapon_type = instant
	projectile_gfx = "sw_proton_torpedo_s"
	power = -1
	cost = 25
		
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 8 max = 10 }
	cooldown = 20
	range = @bomber_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 0 
	shield = 100
	evasion = 0.5
	
	speed = @bomber_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0
	armor_penetration = 0.5
	
	ship_behavior = "bombers_behavior"
	
	icon = "GFX_ship_part_y_wing"
	icon_frame = 1
	component_set = "Y_WING"
	prerequisites = { "tech_rebel_snub_1" }
	tags = { weapon_type_strike_craft }
	upgrades_to = "ADV_Y_WING"
}

strike_craft_component_template = {
	key = "ADV_Y_WING"
	size = LARGE
	entity = "rebel_01_adv_y_wing_entity"

	weapon_type = instant
	projectile_gfx = "sw_proton_torpedo_s"
	power = -1
	cost = 25
		
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 9 max = 11 }
	cooldown = 20
	range = @bomber_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 1 
	shield = 125
	evasion = 0.5
	
	speed = @bomber_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0
	armor_penetration = 0.5
	
	ship_behavior = "bombers_behavior"
	
	icon = "GFX_ship_part_adv_y_wing"
	icon_frame = 1
	component_set = "ADV_Y_WING"
	prerequisites = { "tech_rebel_snub_3" }
	tags = { weapon_type_strike_craft }
	upgrades_to = "B_WING"
}

strike_craft_component_template = {
	key = "B_WING"
	size = LARGE
	entity = "rebel_01_B_Wing_entity"

	weapon_type = instant
	projectile_gfx = "sw_proton_torpedo_s"
	power = -1
	cost = 25
		
	count = 12
	regeneration_per_day = @regen
	launch_time = @launch_time
	
	damage = { min = 10 max = 12 }
	cooldown = 20
	range = @bomber_attack_range
	accuracy = 0.70
	tracking = 0.25
	
	health = 100
	armor = 2 
	shield = 150
	evasion = 0.5
	
	speed = @bomber_speed
	rotation_speed = 0.1
	acceleration = 0.5
	
	shield_damage = 1
	shield_penetration = 0
	armor_penetration = 0.5
	
	ship_behavior = "bombers_behavior"
	
	icon = "GFX_ship_part_b_wing"
	icon_frame = 1
	component_set = "B_WING"
	prerequisites = { "tech_rebel_snub_4" }
	tags = { weapon_type_strike_craft }
}