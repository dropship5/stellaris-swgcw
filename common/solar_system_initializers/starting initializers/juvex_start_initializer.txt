@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

juvex_spawn_initializer = {
	name = "Juvex"
	class = "sc_a"
	usage = custom_empire
	init_effect = { log = "juvex homeworld" }
	max_instances = 1
	flags = { juvex_homeworld }
	planet = { name = "Juvex Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	planet = { name = "Yowhiegawa" class = "pc_molten" orbit_distance = 50 size = 8 has_ring = no }
	planet = { name = "Zupriri" class = random_non_colonizable orbit_distance = 40 size = 10 has_ring = no }
	planet = { name = "Iaulia" class = random_non_colonizable orbit_distance = 40 size = 14 has_ring = no }
	planet = {
		name = "Juvex"
		class = pc_continental
		orbit_distance = 40
		orbit_angle = 20
		size = { min = 21 max = 24 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = juvex_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = juvex_sector } } }
				create_species = { name = "Human" class = JUV portrait = human homeworld = THIS traits = { trait = "trait_deviants" trait = "trait_quick_learners" trait = "trait_thrifty" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = juvpop }
				create_country = {
					name = JuvexSector
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_slaver_guilds" civic = "civic_distinguished_admiralty" }
					authority = auth_oligarchic
					name_list = "HUM3"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_militarist" ethic = "ethic_materialist" }
					species = event_target:juvpop
					flag = random
				}
				last_created_country = {
					set_country_flag = juvex_sector
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = juvex_sector
					set_graphical_culture = misc_02
					give_technology = { tech = "tech_cis_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "pointy" file = "flag_pointy_17.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "purple" "purple" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = juvex_sector }
				save_global_event_target_as = juvex_sector
				give_technology = { tech = "tech_cis_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = juvpop }
			}
			random_country = {
				limit = { has_country_flag = juvex_sector }
				save_event_target_as = juvex_sector
			}
			set_owner = event_target:juvex_sector
			if = {
				limit = { NOT = { exists = event_target:juvtwilek } }
				create_species = {
					name = "Twi'lek"
					plural = "Twi'leks"
					class = "TWI"
					portrait = "twilek"
					name_list="HUM3"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_desert" }
				}
				last_created_species = { 
					save_global_event_target_as = juvtwilek 
					set_citizenship_type = { country = event_target:juvex_sector type = citizenship_slavery }
				}
			}
			if = {
				limit = { NOT = { exists = event_target:juvsullustan } }
				create_species = {
					name = "Sullustan"
					plural = "Sullustans"
					class = "SUL"
					portrait = "sullustan"
					name_list="REP2"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_desert" }
				}
				last_created_species = { 
					save_global_event_target_as = juvsullustan 
					set_citizenship_type = { country = event_target:juvex_sector type = citizenship_slavery }
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:juvex_sector }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:juvtwilek }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:juvsullustan }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:juvex_sector }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:juvex_sector }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:juvex_sector }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:juvex_sector }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:juvsullustan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:juvtwilek }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:juvtwilek }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:juvsullustan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:juvex_sector }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:juvex_sector }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:juvex_sector }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:juvex_sector }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 2 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 2 replace = yes }				
			}
		}
		moon = { class = random_non_colonizable size = 9 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Spuqatera"
		class = pc_gas_giant
		orbit_distance = 100
		size = 31
		has_ring = yes
		moon = { class = random_non_colonizable size = 4 orbit_distance = 17 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Doshienia"
		class = random_non_colonizable
		orbit_distance = 61
		size = { min = 8 max = 15 }
		has_ring = no
		moon = { class = random_non_colonizable size = 6 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Meabos"
		class = random_non_colonizable
		orbit_distance = 52
		size = { min = 8 max = 15 }
		has_ring = yes
	}
}

pirralor_system_initializer = {
	name = "Pirralor"
	class = "sc_g"
	usage = custom_empire
	asteroids_distance = 200
	init_effect = { log = "pirralor system" }
	planet = { name = "Pirralor Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 30 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 1 max = 2 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = {
		name = "Yugleaphus"
		class = random_non_colonizable
		orbit_distance = -150
		size = 11
		has_ring = no 
	}
	planet = {
		name = "Egrars"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no 
	}
	planet = {
		name = "Pirralor"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 150
		size = { min = 15 max = 20 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:juvex_sector }
				set_owner = event_target:juvex_sector
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:juvex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:juvsullustan }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:juvtwilek }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:juvsullustan }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }		
					create_pop = { species = event_target:juvex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvex_sector }					
				}
			}
		}
	}	
	planet = {
		name = "Pluecarro"
		class = "pc_gas_giant"
		orbit_distance = 100
		size = 35
		has_ring = yes
		moon = { class = random_non_colonizable size = 6 orbit_distance = 18 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
	}	
	planet = {
		name = "Geinides"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = yes
	}	
	planet = {
		name = "Tienope"
		class = random_non_colonizable
		orbit_distance = 50
		size = 11
		has_ring = no
	}	
}

pieldi_system_initializer = {
	name = "Pieldi"
	class = "sc_g"
	usage = custom_empire
	asteroids_distance = 200
	init_effect = { log = "pieldi system" }
	planet = { name = "Pieldi Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 30 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 3 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = {
		name = "Glulatis"
		class = random_non_colonizable
		orbit_distance = -150
		size = 9
		has_ring = no 
	}
	planet = {
		name = "Plaater I"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no 
	}
	planet = {
		name = "Pieldi"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 150
		size = { min = 18 max = 23 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:juvex_sector }
				set_owner = event_target:juvex_sector
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:juvex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:juvsullustan }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:juvtwilek }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:juvsullustan }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }		
					create_pop = { species = event_target:juvex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = energy amount = 1 replace = yes }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = energy amount = 1 replace = yes }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = minerals amount = 1 replace = yes }				
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = food amount = 1 replace = yes }				
				}
			}
		}
	}
	planet = {
		name = "Plaater II"
		class = random_non_colonizable
		orbit_distance = 100
		size = 15
		has_ring = no
	}
	planet = {
		name = "Plaater III"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no
	}	
	planet = {
		name = "Loizuno"
		class = random_non_colonizable
		orbit_distance = 50
		size = 11
		has_ring = no
	}	
}

kassido_system_initializer = {
	name = "Kassido"
	class = "sc_b"
	usage = custom_empire
	init_effect = { log = "kassido system" }
	planet = { name = "Kassido Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Brade 38"
		class = random_non_colonizable
		orbit_distance = 50
		size = 7
		has_ring = no 
	}
	planet = {
		name = "Smugolara"
		class = random_non_colonizable
		orbit_distance = 25
		size = 12
		has_ring = no 
	}
	planet = {
		name = "Prejonope"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no 
	}
	planet = {
		name = "Kassido"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 150
		size = { min = 18 max = 23 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:juvex_sector }
				set_owner = event_target:juvex_sector
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:juvex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:juvsullustan }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:juvtwilek }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:juvex_sector }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:juvsullustan }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }		
					create_pop = { species = event_target:juvex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:juvex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvtwilek }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:juvex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = energy amount = 1 replace = yes }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = energy amount = 1 replace = yes }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = minerals amount = 1 replace = yes }				
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = food amount = 1 replace = yes }				
				}
			}
		}
	}
	planet = {
		name = "Zutera"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no
	}
	planet = {
		name = "Aglerth"
		class = random_non_colonizable
		orbit_distance = 50
		size = 13
		has_ring = no
	}	
	planet = {
		name = "Foglalia"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no
	}	
}