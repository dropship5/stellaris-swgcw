@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

trandoshan_spawn_initializer = {
	name = "Kashyyyk"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "trandoshan homeworld" }
	asteroids_distance = 200
	max_instances = 1
	flags = { trandoshan_homeworld }
	planet = { name = "Kashyyyk Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 2 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 70 max = 100 } }
	planet = {
		name = "Taakarroo"
		class = "pc_molten"
		orbit_distance = -150
		size = 8
		has_ring = no
		moon = { class = random_non_colonizable size = 2 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Kallalarra"
		class = "pc_toxic"
		orbit_distance = 60
		size = 12
		has_ring = no
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Trandosha"
		class = pc_arid
		orbit_distance = 140
		orbit_angle = 20
		size = { min = 18 max = 22 }
		starting_planet = yes
		has_ring = yes
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = trandoshan_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = trandoshan_dominion } } }
				create_species = { name = "Trandoshan" class = TRA portrait = rep9 homeworld = THIS traits = { trait = "trait_solitary" trait = "trait_slow_breeders" trait = "trait_very_strong" trait = "trait_enduring" ideal_planet_class = "pc_arid" } }
				last_created_species = { save_global_event_target_as = trapop }
				create_country = {
					name = TrandoshanDominion
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_cutthroat_politics" civic = "civic_slaver_guilds" }
					authority = auth_imperial
					name_list = "REP1"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_militarist" ethic = "ethic_spiritualist" }
					species = event_target:trapop
					flag = random
				}
				last_created_country = {
					set_country_flag = trandoshan_dominion
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = trandoshan_dominion
					set_graphical_culture = misc_01
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "starwars" file = "trandoshan_01.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "dark_green" "dark_green" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = trandoshan_dominion }
				save_global_event_target_as = trandoshan_dominion
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = trapop }
			}
			random_country = {
				limit = { has_country_flag = trandoshan_dominion }
				save_event_target_as = trandoshan_dominion
			}
			set_owner = event_target:trandoshan_dominion
			if = {
				limit = { NOT = { exists = event_target:trawookie } }
				create_species = {
					name = "Wookie"
					plural = "Wookies"
					class = "WOO"
					portrait = "wookie"
					name_list="REP1"
					traits = { trait = "trait_solitary" trait = "trait_slow_breeders" trait = "trait_very_strong" trait = "trait_enduring" ideal_planet_class = "pc_tropical" }
				}
				last_created_species = { 
					save_global_event_target_as = trawookie 
					set_citizenship_type = { country = event_target:trandoshan_dominion type = citizenship_slavery }
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 2 replace = yes }
				create_pop = { species = event_target:trandoshan_dominion ethos = owner }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 2 replace = yes }
					create_pop = { species = event_target:trandoshan_dominion ethos = owner }
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:trawookie ethos = owner }
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 2 replace = yes }
					create_pop = { species = event_target:trandoshan_dominion ethos = owner }
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:trandoshan_dominion ethos = owner }
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }
				create_pop = { species = event_target:trandoshan_dominion }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }
				create_pop = { species = event_target:trawookie }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"	
				add_resource = { resource = minerals amount = 1 replace = yes }		
				create_pop = { species = event_target:trawookie ethos = owner }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:trandoshan_dominion }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:trandoshan_dominion }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = society_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:trandoshan_dominion }	
			}
		}
		moon = { name = "Akoshissss" class = "pc_barren" size = 4 orbit_distance = 9 has_ring = no }
		moon = {
			name = "Wasskah"
			class = "pc_continental"
			orbit_distance = 4
			size = 7
			tile_blockers = none
			has_ring = no		
			init_effect = {
				if = {
					limit = { exists = event_target:trandoshan_dominion }
					set_owner = event_target:trandoshan_dominion
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:trandoshan_dominion ethos = owner }		
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 1 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_power_plant_1"
							add_resource = { resource = energy amount = 1 replace = yes }	
							create_pop = { species = event_target:trandoshan_dominion ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 1 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }	
						}
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }
						create_pop = { species = event_target:trandoshan_dominion }
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }
						create_pop = { species = event_target:trawookie }
					}
				}
			}
		}
	}
	planet = {
		name = "Kashyyyk"
		class = "pc_tropical"
		orbit_distance = -100
		orbit_angle = 20
		size = { min = 18 max = 24 }
		tile_blockers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:trandoshan_dominion }
				set_owner = event_target:trandoshan_dominion
					random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:trawookie }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }
						create_pop = { species = event_target:trawookie }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }
						create_pop = { species = event_target:trawookie }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }
						create_pop = { species = event_target:trawookie }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }
						create_pop = { species = event_target:trawookie }
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trawookie }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:trawookie }				
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:trawookie }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:trawookie }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }	
					create_pop = { species = event_target:trawookie }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }		
					create_pop = { species = event_target:trawookie }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:trawookie }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trawookie }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = energy amount = 1 replace = yes }				
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = food amount = 1 replace = yes }				
				}
			}
		}
		moon = { class = random_non_colonizable size = 4 orbit_distance = 9 has_ring = no }
		moon = {
			name = "Farming Moon"
			class = "pc_continental"
			orbit_distance = 4
			size = 6
			tile_blockers = none
			has_ring = no		
			init_effect = {
				if = {
					limit = { exists = event_target:trandoshan_dominion }
					set_owner = event_target:trandoshan_dominion
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 2 replace = yes }		
						create_pop = { species = event_target:trawookie ethos = owner }		
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 1 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_power_plant_1"
							add_resource = { resource = energy amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 2 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }	
						}
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:trawookie }					
					}
				}
			}
		}
		moon = {
			name = "Farming Moon"
			class = "pc_continental"
			orbit_distance = 4
			size = 6
			tile_blockers = none
			has_ring = no		
			init_effect = {
				if = {
					limit = { exists = event_target:trandoshan_dominion }
					set_owner = event_target:trandoshan_dominion
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 2 replace = yes }		
						create_pop = { species = event_target:trawookie ethos = owner }		
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 1 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_power_plant_1"
							add_resource = { resource = energy amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 2 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }	
						}
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:trawookie }					
					}
				}
			}
		}
	}
	planet = {
		name = "Alaris"
		class = "pc_gas_giant"
		orbit_distance = 160
		orbit_angle = -13
		size = 25
		has_ring = no
		moon = { class = random_non_colonizable size = 7 orbit_distance = 15 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = {
			name = "Alaris Prime"
			class = "pc_tropical"
			orbit_distance = 8
			size = { min = 9 max = 9 }
			tile_blockers = none
			has_ring = no	
			init_effect = {
				if = {
					limit = { exists = event_target:trandoshan_dominion }
					set_owner = event_target:trandoshan_dominion
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:trandoshan_dominion ethos = owner }		
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }	
							create_pop = { species = event_target:trawookie ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 2 replace = yes }		
							create_pop = { species = event_target:trandoshan_dominion ethos = owner }	
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_power_plant_1"
							add_resource = { resource = energy amount = 1 replace = yes }	
							create_pop = { species = event_target:trandoshan_dominion ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 1 replace = yes }		
							create_pop = { species = event_target:trawookie ethos = owner }	
						}
					}
				}
			}
		}
		moon = { class = random_non_colonizable size = 5 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Shurr"
		class = "pc_gas_giant"
		orbit_distance = 60
		size = 23
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 9 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
	}
	planet = {
		name = "Kuhurrik"
		class = "pc_gas_giant"
		orbit_distance = 60
		size = 22
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 9 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = yes }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 4 has_ring = no }
	}
}