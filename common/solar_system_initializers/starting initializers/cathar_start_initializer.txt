@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

cathar_spawn_initializer = {
	name = "Cathar"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "cathar homeworld" }
	asteroids_distance = 150
	max_instances = 1
	flags = { cathar_homeworld }
	planet = { name = "Cathar Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 150
	planet = { count = { min = 3 max = 6 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { name = "Feapra" class = random_non_colonizable orbit_distance = -100 size = 13 has_ring = no }
	planet = { name = "Vusnore" class = random_non_colonizable orbit_distance = 25 size = 13 has_ring = no }
	planet = {
		name = "Oytov"
		class = random_non_colonizable
		orbit_distance = 25
		size = { min = 11 max = 13 }
		has_ring = no
		moon = { class = random_non_colonizable size = 9 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = no }
	}
	planet = {
		name = "Cathar"
		class = pc_continental
		orbit_distance = 25
		orbit_angle = 30
		size = { min = 18 max = 21 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = cathar_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = cathar } } }
				create_species = { name = "Cathar" class = CAT portrait = cathar homeworld = THIS traits = { trait = "trait_solitary" trait = "trait_very_strong" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = catpop }
				create_country = {
					name = GreaterJavin
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_environmentalist" civic = "civic_shadow_council" }
					authority = auth_oligarchic
					name_list = "HUM3"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_xenophile" ethic = "ethic_spiritualist" }
					species = event_target:catpop
					flag = random
				}
				last_created_country = {
					set_country_flag = cathar
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = cathar
					set_graphical_culture = misc_01
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "domination" file = "domination_9.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "burgundy" "burgundy" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = cathar }
				save_global_event_target_as = cathar
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = catpop }
			}
			random_country = {
				limit = { has_country_flag = cathar }
				save_event_target_as = cathar
			}
			set_owner = event_target:cathar
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:cathar }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:cathar }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:cathar }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:cathar }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:cathar }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:cathar }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:cathar }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:cathar }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:cathar }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:cathar }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:cathar }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:cathar }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:cathar }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 2 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 2 replace = yes }		
			}
		}
		moon = { class = random_non_colonizable size = 10 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Voyliv"
		class = random_non_colonizable
		orbit_distance = 55
		size = { min = 11 max = 13 }
		has_ring = no
	}
	planet = {
		name = "Iesworth"
		class = random_non_colonizable
		orbit_distance = 54
		size = { min = 10 max = 11 }
		has_ring = no
		moon = { class = random_non_colonizable size = 8 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Truoter"
		class = random_non_colonizable
		orbit_distance = 61
		size = { min = 8 max = 11 }
		has_ring = no
	}
	planet = {
		name = "Fourilia"
		class = random_non_colonizable
		orbit_distance = 51
		size = { min = 8 max = 11 }
		has_ring = no
	}
}