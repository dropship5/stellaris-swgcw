@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

pantora_spawn_initializer = {
	name = "Pantora"
	class = "sc_k"
	usage = custom_empire
	init_effect = { log = "pantora homeworld" }
	asteroids_distance = 150
	max_instances = 1
	flags = { pantora_homeworld }
	planet = { name = "Pantora Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	change_orbit = 150
	planet = { count = { min = 4 max = 6 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { 
		name = "Uthoria" 
		class = random_non_colonizable 
		orbit_distance = -100
		size = 9 
		has_ring = no 
	}
	planet = {
		name = "Orto Plutonia"
		class = "pc_arctic"
		orbit_distance = 50
		orbit_angle = 20
		size = { min = 19 max = 23 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = pantora_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = pantoran_assembly } } }
				create_species = { 
					name = "Pantoran" 
					class = PAN 
					portrait = pantoran 
					homeworld = THIS 
					traits = {
						trait = "trait_slow_breeders"
						trait = "trait_adaptive"
						trait = "trait_resilient"
						ideal_planet_class = "pc_tropical" 
					} 
				}
				last_created_species = { save_global_event_target_as = zabpop }
				create_country = {
					name = PantoranAssembly
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_environmentalist" civic = "civic_meritocracy" }
					authority = auth_democratic
					name_list = "HUM3"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_xenophile" ethic = "ethic_pacifist" }
					species = event_target:zabpop
					flag = random
				}
				last_created_country = {
					set_country_flag = pantoran_assembly
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = pantoran_assembly
					set_graphical_culture = misc_03
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }
					change_country_flag = {
						icon = { category = "starwars" file = "pantoran_01.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "grey" "grey" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = pantoran_assembly }
				save_global_event_target_as = pantoran_assembly
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = zabpop }
			}
			random_country = {
				limit = { has_country_flag = pantoran_assembly }
				save_event_target_as = pantoran_assembly
			}
			set_owner = event_target:pantoran_assembly
			if = {
				limit = { NOT = { exists = event_target:talz } }
				create_species = {
					name = "Talz"
					plural = "Talz"
					class = "TAL"
					portrait = "talz"
					name_list = "REP2"
					traits = { trait = "trait_sedentary" trait = "trait_adaptive" trait = "trait_strong" ideal_planet_class = "pc_arctic" }
				}
				last_created_species = {
					save_global_event_target_as = talz
					set_citizenship_type = { country = event_target:pantoran_assembly type = citizenship_full }
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:talz }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:talz }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:talz }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:talz }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:talz }	
				}
			}
		}
		moon = {
			name = "Pantora"
			class = pc_tropical
			orbit_distance = 12
			orbit_angle = 40
			size = 15
			has_ring = no
			tile_blockers = none
			modifiers = none
			init_effect = { prevent_anomaly = yes }
			init_effect = {
				if = {
					limit = { exists = event_target:pantoran_assembly }
					set_owner = event_target:pantoran_assembly
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:pantoran_assembly ethos = owner }			
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no has_grown_pop = no has_growing_pop = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }
							create_pop = { species = event_target:pantoran_assembly ethos = owner }	
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no has_grown_pop = no has_growing_pop = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 1 replace = yes }
							create_pop = { species = event_target:pantoran_assembly ethos = owner }	
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no has_grown_pop = no has_growing_pop = no }
							set_building = "building_power_plant_1"
							add_resource = { resource = energy amount = 1 replace = yes }
							create_pop = { species = event_target:pantoran_assembly ethos = owner }	
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"
							add_resource = { resource = minerals amount = 1 replace = yes }		
							create_pop = { species = event_target:pantoran_assembly }	
						}
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:pantoran_assembly }	
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:pantoran_assembly }				
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:pantoran_assembly }					
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:pantoran_assembly }					
					}	
					random_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:pantoran_assembly }					
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						add_resource = { resource = engineering_research amount = 1 replace = yes }
						set_building = "building_basic_science_lab_1"	
						create_pop = { species = event_target:pantoran_assembly }	
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						add_resource = { resource = physics_research amount = 1 replace = yes }	
						set_building = "building_basic_science_lab_1"
						create_pop = { species = event_target:pantoran_assembly }					
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						add_resource = { resource = physics_research amount = 1 replace = yes }	
						set_building = "building_basic_science_lab_1"
						create_pop = { species = event_target:pantoran_assembly }					
					}
					random_tile = {
						limit = { has_blocker = no has_building = no }
						add_resource = { resource = energy amount = 1 replace = yes }
					}
				}
			}
		}
	}
	planet = {
		name = "Dragonia"
		class = pc_gas_giant
		orbit_distance = 120
		size = { min = 19 max = 22 }
		has_ring = yes
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
	}
	planet = {
		name = "Drucegantu"
		class = random_non_colonizable
		orbit_distance = 70
		size = { min = 22 max = 25 }
		has_ring = no
		moon = { class = random_non_colonizable size = 6 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Dautania"
		class = random_non_colonizable
		orbit_distance = 70
		size = { min = 10 max = 15 }
		has_ring = no
	}
}