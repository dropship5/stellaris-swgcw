@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

zygerrian_spawn_initializer = {
	name = "Zygerria"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "zygerrian homeworld" }
	max_instances = 1
	flags = { zygerrian_homeworld }
	planet = { name = "Zygerria Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = { name = "Leslapus" class = "pc_molten" orbit_distance = 53 size = 7 has_ring = no }
	planet = {
		name = "Zygerria"
		class = "pc_arid"
		orbit_distance = 102
		orbit_angle = 20
		size = { min = 23 max = 25 }
		starting_planet = yes
		has_ring = yes
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = zygerrian_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = zygerrian_slave_guild } } }
				create_species = { name = "Zygerrian" class = ZYG portrait = mam3 homeworld = THIS traits = { trait = "trait_decadent" trait = "trait_rapid_breeders" trait = "trait_thrifty" ideal_planet_class = "pc_arid" } }
				last_created_species = { save_global_event_target_as = zygpop }
				create_country = {
					name = ZygerrianSlaverGuild
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_slaver_guilds" civic = "civic_police_state" }
					authority = auth_dictatorial
					name_list = "MAM2"
					ethos = { ethic = "ethic_fanatic_authoritarian" ethic = "ethic_xenophobe" }
					species = event_target:zygpop
					flag = random
				}
				last_created_country = {
					set_country_flag = zygerrian_slave_guild
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = zygerrian_slave_guild
					set_graphical_culture = misc_02
					give_technology = { tech = "tech_cis_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "zoological" file = "flag_zoological_20.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "yellow" "yellow" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = zygerrian_slave_guild }
				save_global_event_target_as = zygerrian_slave_guild
				give_technology = { tech = "tech_cis_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = zygpop }
			}
			random_country = {
				limit = { has_country_flag = zygerrian_slave_guild }
				save_event_target_as = zygerrian_slave_guild
			}
			set_owner = event_target:zygerrian_slave_guild
			if = {
				limit = { NOT = { exists = event_target:zyghuman } }
				create_species = {
					name = "Human"
					plural = "Humans"
					class = "HUM"
					portrait = "human"
					name_list="HUM4"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_continental" }
				}
				last_created_species = {
					save_global_event_target_as = zyghuman
					set_citizenship_type = { country = event_target:husnock_ascendency type = citizenship_caste_system }
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:zygerrian_slave_guild }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:zyghuman }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:zyghuman }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:zygerrian_slave_guild }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:zyghuman }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:zygerrian_slave_guild }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:zygerrian_slave_guild }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:zyghuman }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = minerals amount = 2 replace = yes }	
				create_pop = { species = event_target:zyghuman }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:zyghuman }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:zyghuman }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:zygerrian_slave_guild }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:zygerrian_slave_guild }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = energy amount = 1 replace = yes }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = food amount = 1 replace = yes }				
			}
		}
		moon = { class = "pc_barren" size = 4 orbit_distance = 8 has_ring = no }
	}
	planet = {
		name = "Uyvis"
		class = "pc_desert"
		orbit_distance = 54
		orbit_angle = 100
		size = { min = 8 max = 11 }
		tile_blockers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:zygerrian_slave_guild }
				set_owner = event_target:zygerrian_slave_guild
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:zygerrian_slave_guild ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:zyghuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:zygerrian_slave_guild ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"	
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:zyghuman ethos = owner }	
					}
				}
			}
		}
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
	}	
	planet = {
		name = "Brelumia"
		class = "pc_gas_giant"
		orbit_distance = 47
		size = 25
		has_ring = no
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
	}
	planet = { name = "Yaynov" class = random_non_colonizable size = 10 orbit_distance = 42 has_ring = no }
	planet = { name = "Na" class = random_non_colonizable size = 7 orbit_distance = 42 has_ring = no }
}

kadavo_system_initializer = {
	name = "Kadavo"
	class = "sc_a"
	usage = custom_empire
	init_effect = { log = "kadavo system" }
	planet = { name = "Kadavo Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 40 has_ring = no }
	planet = {
		name = "Ubliovis"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no
	}
	planet = {
		name = "Togreynides"
		class = random_non_colonizable
		orbit_distance = 25
		size = 14
		has_ring = no
	}
	planet = {
		name = "Ietrinda"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no
	}
	planet = {
		name = "Kadavo"
		class = "pc_arid"
		orbit_distance = 50
		orbit_angle = 50
		size = { min = 15 max = 19 }
		tile_blockers = none
		modifiers = none
		has_ring = no
		init_effect = {
			if = {
				limit = { exists = event_target:zygerrian_slave_guild }
				set_owner = event_target:zygerrian_slave_guild
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 2 replace = yes }	
					create_pop = { species = event_target:zygerrian_slave_guild }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:zyghuman }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:zyghuman }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:zygerrian_slave_guild }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:zyghuman }
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = minerals amount = 1 replace = yes }	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }	
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
			}
		}
	}
	planet = {
		name = "Cufluna"
		class = "pc_gas_giant"
		orbit_distance = 75
		size = 31
		has_ring = no
		moon = { class = random_non_colonizable size = 3 orbit_distance = 13 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 0 has_ring = no }
	}
	planet = {
		name = "Qialia"
		class = random_non_colonizable
		orbit_distance = 25
		size = 11
		has_ring = no
	}
}

listehol_system_initializer = {
	name = "Listehol"
	class = "sc_k"
	usage = custom_empire
	init_effect = { log = "listehol system" }
	planet = { name = "Listehol Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 40 has_ring = no }
	planet = {
		name = "Giyhiri"
		class = random_non_colonizable
		orbit_distance = 75
		size = 9
		has_ring = no
	}
	planet = {
		name = "Swasoter"
		class = random_non_colonizable
		orbit_distance = 50
		size = 13
		has_ring = no
	}
	planet = {
		name = "Listehol"
		class = "pc_desert"
		orbit_distance = 50
		orbit_angle = 150
		size = { min = 20 max = 23 }
		tile_blockers = none
		modifiers = none
		has_ring = no
		init_effect = {
			if = {
				limit = { exists = event_target:zygerrian_slave_guild }
				set_owner = event_target:zygerrian_slave_guild
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:zygerrian_slave_guild }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:zyghuman }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:zyghuman }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:zygerrian_slave_guild }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 3 replace = yes }		
						create_pop = { species = event_target:zyghuman }
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 2 replace = yes }		
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = minerals amount = 1 replace = yes }	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }	
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:zyghuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:zygerrian_slave_guild }
				}
			}
		}
	}
	planet = {
		name = "Smozostea"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 33
		has_ring = no
		moon = { class = random_non_colonizable size = 3 orbit_distance = 13 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 1 orbit_distance = 0 has_ring = no }
	}
	planet = {
		name = "Trorix"
		class = random_non_colonizable
		orbit_distance = 50
		size = 10
		has_ring = no
	}
}