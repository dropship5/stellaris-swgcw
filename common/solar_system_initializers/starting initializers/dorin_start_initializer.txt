@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

dorin_spawn_initializer = {
	name = "Dorin"
	class = "sc_a"
	usage = custom_empire
	init_effect = { log = "dorin homeworld" }
	asteroids_distance = 200
	max_instances = 1
	flags = { dorin_homeworld }
	planet = { name = "Dorin Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 3 max = 5 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 40 max = 100 } }	
	planet = {
		name = "Pospuwei"
		class = "pc_continental"
		orbit_distance = -100
		size = 15
		has_ring = yes
		moon = { class = random_non_colonizable size = 4 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 4 has_ring = no }
	}
	planet = {
		name = "Dorin"
		class = pc_continental
		orbit_distance = 50
		orbit_angle = 50
		size = { min = 20 max = 23 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = dorin_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = deadalis_sector } } }
				create_species = { name = "Kel'Dor" class = KEL portrait = keldor homeworld = THIS traits = { trait = "trait_sedentary" trait = "trait_conformists" trait = "trait_communal" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = kelpop }
				create_country = {
					name = DeadalisSector
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_idealistic_foundation" civic = "civic_environmentalist" }
					authority = auth_democratic
					name_list = "HUM3"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_xenophile" ethic = "ethic_pacifist" }
					species = event_target:kelpop
					flag = random
				}
				last_created_country = {
					set_country_flag = deadalis_sector
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = deadalis_sector
					set_graphical_culture = misc_03
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }
					change_country_flag = {
						icon = { category = "ornate" file = "flag_ornate_3.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "dark_brown" "dark_brown" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = deadalis_sector }
				save_global_event_target_as = deadalis_sector
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = kelpop }
			}
			random_country = {
				limit = { has_country_flag = deadalis_sector }
				save_event_target_as = deadalis_sector
			}
			set_owner = event_target:deadalis_sector
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:deadalis_sector }
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:deadalis_sector }
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:deadalis_sector }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:deadalis_sector }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }
				create_pop = { species = event_target:deadalis_sector }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:deadalis_sector }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
		}
		moon = { class = random_non_colonizable size = 7 orbit_distance = 11 has_ring = no }
	}	
	planet = {
		name = "Godrolla"
		class = "pc_barren"
		orbit_distance = 120
		size = 15
		has_ring = yes
		moon = { class = random_non_colonizable size = 4 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Chazarilia"
		class = "pc_gas_giant"
		orbit_distance = 70
		size = { min = 26 max = 30 }
		has_ring = no
		moon = { class = random_non_colonizable size = 6 orbit_distance = 13 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
}