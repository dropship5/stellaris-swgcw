#Concussion Missiles

component_set = { key = "CONCUSSION_MISSILE_1" icon = "GFX_ship_part_concussion_missile_1" icon_frame = 1 }
component_set = { key = "CONCUSSION_MISSILE_2" icon = "GFX_ship_part_concussion_missile_2" icon_frame = 1 }
component_set = { key = "CONCUSSION_MISSILE_3" icon = "GFX_ship_part_concussion_missile_3" icon_frame = 1 }
component_set = { key = "CONCUSSION_MISSILE_4" icon = "GFX_ship_part_concussion_missile_4" icon_frame = 1 }

#Proton Torpedoes

component_set = { key = "PROTON_TORPEDO_1" icon = "GFX_ship_part_proton_torpedo_1" icon_frame = 1 }
component_set = { key = "PROTON_TORPEDO_2" icon = "GFX_ship_part_proton_torpedo_2" icon_frame = 1 }
component_set = { key = "PROTON_TORPEDO_3" icon = "GFX_ship_part_proton_torpedo_3" icon_frame = 1 }
component_set = { key = "PROTON_TORPEDO_4" icon = "GFX_ship_part_proton_torpedo_4" icon_frame = 1 }